import TRow from "../../model/TRow";

interface TFilterListDTOProps {
    filters?: TRow,
    params?: TRow,
}

class TFilterListDTO {
    filters?: TRow;
    params?: TRow;

    constructor(props: TFilterListDTOProps) {
        this.filters = props.filters;
        this.params = props.params;
    }
}

export default TFilterListDTO;
