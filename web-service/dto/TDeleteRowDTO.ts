import TRow from "../../model/TRow";

interface TDeleteRowDTOProps {
    rowPkFields: TRow,
    params?: TRow,
}

class TDeleteRowDTO {
    rowPkFields: TRow;
    params?: TRow;

    constructor(props: TDeleteRowDTOProps) {
        this.rowPkFields = props.rowPkFields;
        this.params = props.params;
    }
}

export default TDeleteRowDTO;
