import TRow from "../../model/TRow";

interface TRetrieveListAndDataDTOProps {
    params?: TRow,
}

class TRetrieveListAndDataDTO {
    params?: TRow;

    constructor(props: TRetrieveListAndDataDTOProps) {
        this.params = props.params;
    }
}

export default TRetrieveListAndDataDTO;
