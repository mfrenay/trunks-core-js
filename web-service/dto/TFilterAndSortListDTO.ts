import TFilterListDTO from "./TFilterListDTO";
import TSortListDTO from "./TSortListDTO";

interface TFilterAndSortListDTOProps {
    tFilterListDTO?: TFilterListDTO,
    tSortListDTO?: TSortListDTO,
}

class TFilterAndSortListDTO {
    tFilterListDTO?: TFilterListDTO;
    tSortListDTO?: TSortListDTO;

    constructor(props: TFilterAndSortListDTOProps = {}) {
        this.tFilterListDTO = props.tFilterListDTO;
        this.tSortListDTO = props.tSortListDTO;
    }
}

export default TFilterAndSortListDTO;
