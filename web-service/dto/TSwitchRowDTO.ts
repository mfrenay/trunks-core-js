import TRow from "../../model/TRow";

interface TSwitchRowDTOProps {
    rowPkFields: TRow,
    params?: TRow,
}

class TSwitchRowDTO {
    rowPkFields: TRow;
    params?: TRow;

    constructor(props: TSwitchRowDTOProps) {
        this.rowPkFields = props.rowPkFields;
        this.params = props.params;
    }
}

export default TSwitchRowDTO;
