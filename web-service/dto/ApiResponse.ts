export const API_RESPONSE_STATUS = Object.freeze({
    success: "success",
    warning: "warning",
    error: "error"
});


class ApiResponse {
    status: string;
    message: string;
    errors: string[];
    data: any;

    constructor(status: string, message: string, errors: string[], data: any) {
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.data = data;
    }
}

export default ApiResponse;
