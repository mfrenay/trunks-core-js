import TRow from "../../model/TRow";

interface TInsertRowDTOProps {
    row: TRow,
    params?: TRow,
}

class TInsertRowDTO {
    row: TRow;
    params?: TRow;

    constructor(props: TInsertRowDTOProps) {
        this.row = props.row;
        this.params = props.params;
    }
}

export default TInsertRowDTO;
