import TRow from "../../model/TRow";
import TRowState from "../../model/TRowState";
import TField from "../../model/TField";

interface TSaveFormDataDTOProps {
    row: TRow,
    initialValues?: TField[],
    rowState?: TRowState,
    params?: TRow,
}

class TSaveFormDataDTO {
    row: TRow;
    initialValues: TField[];
    rowState?: TRowState;
    params?: TRow;

    constructor(props: TSaveFormDataDTOProps) {
        this.row = props.row;
        this.initialValues = props.initialValues ?? [];
        this.rowState = props.rowState;
        this.params = props.params;
    }
}

export default TSaveFormDataDTO;
