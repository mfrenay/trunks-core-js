
interface TSortListDTOProps {
    sortingFieldId: string,
    sortingOrder: number,
}

class TSortListDTO {
    sortingFieldId: string;
    sortingOrder: number;

    constructor(props: TSortListDTOProps) {
        this.sortingFieldId = props.sortingFieldId;
        this.sortingOrder = props.sortingOrder;
    }
}

export default TSortListDTO;
