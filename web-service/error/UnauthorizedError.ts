class UnauthorizedError extends Error {
  origin?: string; // Name of the function that throw the HttpError

  constructor(message: string, origin?: string) {
    super(message);
    this.origin = origin;
  }
}

export default UnauthorizedError;
