interface IWebServiceManager {
    fetchMethod: (url: RequestInfo, init?: RequestInit) => Promise<Response>,
    manageAuthorizationResponse: (response: Response, origin: string) => Promise<any>,
    getIdUser: () => string | null
}

export default IWebServiceManager;
