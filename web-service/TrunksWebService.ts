import Logger from "../logger/logger";
import TList from "../model/TList";
import TRow from "../model/TRow";
import TForm from "../model/TForm";

import TSaveFormDataDTO from "./dto/TSaveFormDataDTO";
import TFilterListDTO from "./dto/TFilterListDTO";
import TRetrieveListAndDataDTO from "./dto/TRetrieveListAndDataDTO";
import TSwitchRowDTO from "./dto/TSwitchRowDTO";
import TDeleteRowDTO from "./dto/TDeleteRowDTO";

import ApiResponse, {API_RESPONSE_STATUS} from "./dto/ApiResponse";
import {config} from "../config";
import WebServiceUtils from "../utilities/WebServiceUtils";
import TFilterAndSortListDTO from "./dto/TFilterAndSortListDTO";
import FileSaver from "file-saver";
import TCForm from "../model/TCForm";
import TField from "../model/TField";
import TInsertRowDTO from "./dto/TInsertRowDTO";

type Callback = (...data: any) => void;
type ErrorCallback = (jsonResponse: ApiResponse) => void;

/**
 * Trunks Generic Web Services
 */
class TrunksWebService {

  webFormApiUrl: string;
  constructor(webFormApiUrl?: string) {
    const url = webFormApiUrl ?? config.WEB_FORM_API_URL;
    if (url == null) {
        throw new Error("You must give a parameter to the constructor of TrunksWebService or set config.WEB_FORM_API_URL");
    }
    this.webFormApiUrl = url;
  }

  /**
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param list
     * @param params
     */
    async saveList(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, list: TList, params?: TRow) {
        Logger.d(`TrunksWebService.saveList(${dataSourceId}, ${list})`);
        Logger.d(JSON.stringify(list));

        const url = this.webFormApiUrl + "/saveList?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
            params: params ? JSON.stringify(params) : "",
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "POST",
            headers: {
                "Accept": "application/json;charset=UTF-8",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(list) // API server will receive this JSON object and automatically convert it into a DTO object
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "saveList"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.saveList() - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    await callback?.();
                } else {
                    Logger.e("TrunksWebService.saveList - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param dataSourceParams
     * @param idUser
     */
    async getListData(callback: Callback, errorCallback: ErrorCallback | null, dataSourceId: string, dataSourceParams?: TRow, idUser?: string) {
        Logger.d(`TrunksWebService.getListData(${dataSourceId})`);

        const url = this.webFormApiUrl + "/getListData?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...(idUser != null ? {idUser: idUser} : getIdUserParam()),
            params: dataSourceParams ? JSON.stringify(dataSourceParams) : ""
        });

        await getWebServiceManager().fetchMethod(url)
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "getListData"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.getListData - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    const tListJson = jsonResponse.data;
                    await callback?.(tListJson.rows.map((row: any) => TRow.fromJson(row)));
                } else {
                    Logger.e("TrunksWebService.getListData - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param primaryKeys
     * @param params
     * @param newFormFallback
     */
    async getFormData(callback: Callback, errorCallback: ErrorCallback | null, dataSourceId: string, primaryKeys: TField[], params?: TRow, newFormFallback: boolean = true) {
        const url = this.webFormApiUrl + "/getFormData?" + WebServiceUtils.buildURIQueryString({
            idForm: dataSourceId,
            ...getIdUserParam(),
            primaryKeys: JSON.stringify(primaryKeys),
            newFormFallback: newFormFallback,
            params: params ? JSON.stringify(params) : ""
        });

        await getWebServiceManager().fetchMethod(url)
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "getFormData"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.getFormData - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    await callback?.(TForm.fromJson(jsonResponse.data));
                }
                else {
                    Logger.e("TrunksWebService.getFormData - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

  /**
   *
   * @param callback
   * @param errorCallback
   * @param dataSourceId
   * @param primaryKeys
   * @param params
   */
    async getCFormData(callback: Callback, errorCallback: ErrorCallback | null, dataSourceId: string, primaryKeys: TField[], params?: TRow) {
      const url = this.webFormApiUrl + "/getCFormData?" + WebServiceUtils.buildURIQueryString({
        idCForm: dataSourceId,
        ...getIdUserParam(),
        primaryKeys: JSON.stringify(primaryKeys),
        params: params ? JSON.stringify(params) : ""
      });

      await getWebServiceManager().fetchMethod(url)
        .then(response => getWebServiceManager().manageAuthorizationResponse(response, "getCFormData"))
        .then(response => response.json())
        .then(async (jsonResponse: ApiResponse) => {
          Logger.d("TrunksWebService.getCFormData - response:", jsonResponse);

          if (jsonResponse.status === API_RESPONSE_STATUS.success) {
            await callback?.(TCForm.fromJson(jsonResponse.data));
          }
          else {
            Logger.e("TrunksWebService.getCFormData - error :", jsonResponse.message);
            await errorCallback?.(jsonResponse);
          }
        });
    }

    /**
     * getNewFormData
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param params
     */
    async getNewFormData(callback: Callback, errorCallback: ErrorCallback | null, dataSourceId: string, params?: TRow) {
        const url = this.webFormApiUrl + "/getNewFormData?" + WebServiceUtils.buildURIQueryString({
            idForm: dataSourceId,
            ...getIdUserParam(),
            params: params ? JSON.stringify(params) : ""
        });

        await getWebServiceManager().fetchMethod(url)
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "getNewFormData"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.getNewFormData - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    await callback?.(TForm.fromJson(jsonResponse.data));
                }
                else {
                    Logger.e("TrunksWebService.getNewFormData - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param tSaveFormDataDTO
     */
    async saveFormData(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, tSaveFormDataDTO: TSaveFormDataDTO) {
        Logger.d(`TrunksWebService.saveFormData(${dataSourceId})...`, tSaveFormDataDTO);

        const url = this.webFormApiUrl + "/saveFormData?" + WebServiceUtils.buildURIQueryString({
            idForm: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "POST",
            headers: {
                "Accept": "application/json;charset=UTF-8",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(tSaveFormDataDTO) // API server will receive this JSON object and automatically convert it into a DTO object
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "saveFormData"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.saveFormData - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success || jsonResponse.status === API_RESPONSE_STATUS.warning) {
                    await callback?.(jsonResponse); // jsonResponse.data.primaryKeys => Example: {ID_MEDICAL_CENTER: "9", ID_USER: "21"}
                } else {
                    Logger.e("TrunksWebService.saveFormData - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     * getMatchingValues
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param searchOnlyStartingWith
     * @param searchText
     * @param params
     */
    async getMatchingValues(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, searchOnlyStartingWith: boolean, searchText: string, params?: TRow) {
        const url = this.webFormApiUrl + "/getMatchingValues?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
            searchOnlyStartingWith: searchOnlyStartingWith,
            searchText: searchText,
            params: params ? JSON.stringify(params) : ""
        });

        await getWebServiceManager().fetchMethod(url)
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "getMatchingValues"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.getMatchingValues() - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    await callback?.(jsonResponse.data);
                }
                else {
                    Logger.e("TrunksWebService.getMatchingValues - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     * Return base definition of a TList object
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     */
    async getListObject(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string) {
        const url = this.webFormApiUrl + "/getListObject?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url)
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "getListObject"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.getListObject - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    await callback?.(TList.fromJson(jsonResponse.data));
                }
                else {
                    Logger.e("TrunksWebService.getListObject - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     * filterList
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param tFilterListDto
     */
    async filterList(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, tFilterListDto: TFilterListDTO) {
        Logger.d(`TrunksWebService.filterList(${dataSourceId})...`, tFilterListDto);

        const url = this.webFormApiUrl + "/filterList?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "POST",
            headers: {
                "Accept": "application/json;charset=UTF-8",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(tFilterListDto),
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "filterList"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.filterList - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    const tListJson = jsonResponse.data;
                    await callback?.(TList.fromJson(tListJson));
                }
                else {
                    Logger.e("TrunksWebService.filterList - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     * filterAndSortList
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param tFilterAndSortListDto
     */
    async filterAndSortList(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, tFilterAndSortListDto: TFilterAndSortListDTO) {
        Logger.d(`TrunksWebService.filterAndSortList(${dataSourceId})...`, tFilterAndSortListDto);

        const url = this.webFormApiUrl + "/filterAndSortList?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url, {
                method: "POST",
                headers: {
                    "Accept": "application/json;charset=UTF-8",
                    "Content-Type": "application/json;charset=UTF-8"
                },
                body: JSON.stringify(tFilterAndSortListDto),
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "filterAndSortList"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.filterAndSortList - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    const tListJson = jsonResponse.data;
                    await callback?.(TList.fromJson(tListJson));
                }
                else {
                    Logger.e("TrunksWebService.filterAndSortList - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     *
     * @param successCallback
     * @param errorCallback
     * @param dataSourceId
     * @param tDeleteRowDTO
     */
    async deleteRow(successCallback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, tDeleteRowDTO: TDeleteRowDTO) {
        const url = this.webFormApiUrl + "/deleteRow?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "DELETE",
            headers: {
                "Accept": "application/json;charset=UTF-8",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(tDeleteRowDTO)
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "deleteRow"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.deleteRow() - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    const responseData = jsonResponse.data;
                    await successCallback?.(responseData.nbDeletedRows);
                }
                else {
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     *
     * @param successCallback
     * @param errorCallback
     * @param dataSourceId
     * @param tInsertRowDTO
     */
    async insertRow(successCallback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, tInsertRowDTO: TInsertRowDTO) {
        const url = this.webFormApiUrl + "/insertRow?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "PUT",
            headers: {
                "Accept": "application/json;charset=UTF-8",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(tInsertRowDTO)
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "insertRow"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.insertRow() - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    //const responseData = jsonResponse.data;
                    await successCallback?.(/*responseData.x*/);
                }
                else {
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     * switchRow
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param _
     * @param direction
     * @param tSwitchRowDTO
     */
    async switchRow(callback: Callback | null,
                    errorCallback: ErrorCallback | null,
                    dataSourceId: string,
                    _: TList,
                    direction: string,
                    tSwitchRowDTO: TSwitchRowDTO) {
        Logger.d("TrunksWebService.switchRow - direction: " + direction);

        const url = this.webFormApiUrl + "/switchRow?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
            direction: direction,
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "POST",
            headers: {
                "Accept": "application/json;charset=UTF-8",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(tSwitchRowDTO)
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "switchRow"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.switchRow - response:", jsonResponse);

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    const tListJson = jsonResponse.data;
                    await callback?.(tListJson.rows.map((row: any) => TRow.fromJson(row)));
                }
                else {
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     * retrieveListAndData
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param tRetrieveListAndDataDTO
     */
    async retrieveListAndData(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, tRetrieveListAndDataDTO?: TRetrieveListAndDataDTO) {
        Logger.d("TrunksWebService.retrieveListAndData - dataSourceId : " + dataSourceId, tRetrieveListAndDataDTO);

        const url = this.webFormApiUrl + "/retrieveListAndData?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "POST",
            headers: {
                "Accept": "application/json;charset=UTF-8",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(tRetrieveListAndDataDTO)
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "retrieveListAndData"))
            .then(response => response.json())
            .then(async (jsonResponse: ApiResponse) => {
                Logger.d("TrunksWebService.retrieveListAndData - response:", jsonResponse);

                const tListJson = jsonResponse.data;
                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    await callback?.(TList.fromJson(tListJson));
                }
                else {
                    Logger.e("TrunksWebService.retrieveListAndData - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }

    /**
     * exportXls
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param tFilterAndSortListDto
     */
    async exportXls(callback: Callback | null, errorCallback: () => void | null, dataSourceId: string, tFilterAndSortListDto: TFilterAndSortListDTO) {
        Logger.d("TrunksWebService.exportXls - dataSourceId : " + dataSourceId, tFilterAndSortListDto);

        const url = this.webFormApiUrl + "/exportXls?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
        });

        await getWebServiceManager().fetchMethod(url, {
            method: "POST",
            headers: {
                //"Accept": "application/octet-stream",
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify(tFilterAndSortListDto)
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "exportXls"))
            .then(res => res.status === 200 ? res.blob() : null)
            .then(async (blob) => {
                Logger.d("TrunksWebService.exportXls - response:");

                if (blob != null) {
                    let fileName = process.env.NEXT_PUBLIC_EXCEL_EXPORT_FILE_NAME ?? "data.xls";
                    if (!fileName.endsWith(".xls")) {
                        fileName = fileName.concat(".xls");
                    }

                    FileSaver.saveAs(blob, fileName);
                    await callback?.();
                }
                else {
                    await errorCallback?.();
                }
            });
    }

    /**
     * importXls
     *
     * @param callback
     * @param errorCallback
     * @param dataSourceId
     * @param file
     * @param params
     */
    async importXls(callback: Callback | null, errorCallback: ErrorCallback | null, dataSourceId: string, file: File, params?: TRow) {
        Logger.d("TrunksWebService.importXls : " + file.name + " - size " + file.size);

        const url = this.webFormApiUrl + "/importXls?" + WebServiceUtils.buildURIQueryString({
            idList: dataSourceId,
            ...getIdUserParam(),
            params: params ? JSON.stringify(params) : ""
        });

        let formData = new FormData();
        formData.append('file', file);

        await getWebServiceManager().fetchMethod(url, {
            method: "POST",
            body: formData
        })
            .then(response => getWebServiceManager().manageAuthorizationResponse(response, "importXls")) // TODO manage authorization
            .then(response => response.json())
            .then(async (jsonResponse) => {
                Logger.d("TrunksWebService.importXls - response:");

                if (jsonResponse.status === API_RESPONSE_STATUS.success) {
                    await callback?.();
                }
                else {
                    Logger.e("TrunksWebService.importXls - error :", jsonResponse.message);
                    await errorCallback?.(jsonResponse);
                }
            });
    }
}

/**
 * Get the web service manager containing methods for fetching the API
 */
function getWebServiceManager() {
    if (config.webServiceManager == null) {
        throw new Error("You must initialize a WebServiceManager using the init() method of trunks-core-js.");
    }

    return config.webServiceManager!;
}

/**
 * Get the idUser param to use in the fetch function
 *
 * @return An empty object in case the auth user is the same as the current user.
 * An object containing the id of the user to use for the query, if the current user is different than the auth user.
 */
function getIdUserParam(): {idUser: string} | {} {
    const idUser = getWebServiceManager().getIdUser();

    return idUser != null ? {idUser: idUser} : {};
}

export default TrunksWebService;
