# trunks-core-js

JavaScript objects defining the core of the Trunks framework.

The main objects are:

- TField
- TRow
- TForm
- TList