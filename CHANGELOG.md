# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2023-06-05
### Added
- InsertRow API method
- Custom param ONLY_SEND_ROWS_ON_FILTER
- Custom param INSERT_ROW_ON_ADD
- Method replaceParametersInExpression in StringUtils
- ntranslate method in TranslationUtils
- Manage special option attribute OPTION_DISABLED
- DateUtils method to format a number of minutes as "x hours and y minutes"
- New custom object param DISPLAY_LABEL

### Changed
- Retrieve form row state from json
