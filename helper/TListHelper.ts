import TRow from "../model/TRow";
import TField from "../model/TField";
import TRowState from "../model/TRowState";

const TListHelper = {

    /**
     *
     * @param tRow
     * @param baseRow
     */
    setRowState(tRow: TRow, baseRow: TRow): boolean {
        const fieldSyncStatus = tRow.getFieldById("SYNC_STATUS");

        if (fieldSyncStatus == null) {
            return true;
        }

        const nonPkFields = [];

        // Check if the field has an empty primary key (if yes, set the row state to NEW)
        for (let i = 0; i < tRow.fields.length; i++) {
            const field = tRow.fields[i] as TField;
            const baseField: TField = baseRow.fields[i] as TField;

            if (baseField.isPrimaryKey) {
                if (field.stringValue === "") {
                    fieldSyncStatus.value = TRowState.NEW;
                    return true;
                }
            }
            else {
                nonPkFields.push(field);
            }
        }

        // Check if one non-PK field has changed (if yes, set the row state to MODIFIED)
        for (let i = 0; i < nonPkFields.length; i++) {
            const field = tRow.fields[i] as TField;

            if (field.hasChanged()) {
                fieldSyncStatus.value = TRowState.MODIFIED;
                return true;
            }
        }

        return false;
    },

    /**
     *
     * @param tRow
     * @param baseRow
     */
    getRowState(tRow: TRow, baseRow: TRow): TRowState {
        for (let i = 0; i < tRow.fields.length; i++) {
            const field = tRow.fields[i] as TField;
            const baseField: TField = baseRow.fields[i] as TField;

            if (baseField.isPrimaryKey) {
                if (field.stringValue === "") {
                    return TRowState.NEW;
                }
            }
            else {
                if (field.hasChanged()) {
                    return TRowState.MODIFIED;
                }
            }
        }

        return TRowState.UNCHANGED;
    },
}

export default TListHelper;
