export enum TOptionAttributeID {
    CLASS_NAME = "CLASS_NAME",
    OPTION_DISABLED = "OPTION_DISABLED",
}

class TOptionAttribute {
    id: TOptionAttributeID | string;
    value: string;

    constructor(id: string, value: string) {
        this.id = id;
        this.value = value;
    }

    static fromJson(json: any): TOptionAttribute {
        return new TOptionAttribute(json.id, json.value);
    }
}

export default TOptionAttribute;
