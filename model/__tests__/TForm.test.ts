import TForm from "../TForm";
import TField from "../TField";

describe("fromJson()", () => {
    it('should return new TForm from JSON object', () => {
        const json = {
            "id": "FORM_ID",
            "baseRow": {
                "fields": [
                    {
                        "id": "FIELD_ID",
                        "value": "",
                        "label": "Field ID",
                        "kind": "TEXTFIELD",
                        "customDisplay": null,
                        "type": "INTEGER",
                        "isPrimaryKey": true,
                        "optionValues": [],
                        "isVisible": false,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    },
                    {
                        "id": "TITLE",
                        "value": "",
                        "label": "eventType:title",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": true,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    }
                ]
            },
            "row": {
                "id": "",
                "fields": [
                    {
                        "id": "FIELD_ID",
                        "value": "51"
                    },
                    {
                        "id": "TITLE",
                        "value": "Prise de sang"
                    },
                ]
            }
        };

        const tForm: TForm = TForm.fromJson(json);

        // Deep types equality check
        expect(tForm.baseRow.fields[0] instanceof TField).toEqual(true);
        expect(tForm.row.fields[0] instanceof TField).toEqual(true);

        expect(tForm.id).toEqual("FORM_ID");

        expect(tForm.baseRow.fields.length).toEqual(2);
        expect(tForm.baseRow.fields[0].id).toEqual("FIELD_ID");
        expect(tForm.row.fields.length).toEqual(2);
        expect(tForm.row.fields[0].value).toEqual("51");
    });
});
