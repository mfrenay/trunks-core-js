import TField from "../TField";
import TList from "../TList";

describe("fromJson()", () => {
    it('should return new TList from JSON object', () => {
        const json = {
            "id": "EVENT_TYPES_LIGHT",
            "baseRow": {
                "fields": [
                    {
                        "id": "ID_EVENT_TYPE",
                        "value": "",
                        "label": "ID_EVENT_TYPE",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": true,
                        "optionValues": [],
                        "isVisible": false,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    },
                    {
                        "id": "ICON",
                        "value": "",
                        "label": "eventType:icon",
                        "kind": "IMAGE",
                        "customDisplay": "FieldEventTypeIcon",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": true,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    },
                    {
                        "id": "DISPLAY_ORDER",
                        "value": "",
                        "label": "eventType:displayOrder",
                        "kind": "TEXTFIELD",
                        "customDisplay": "ORDER",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": true,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": true
                    },
                    {
                        "id": "LABEL",
                        "value": "",
                        "label": "eventType:label",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": true,
                        "hasStartWildcard": true,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": true
                    },
                    {
                        "id": "BACKGROUND_COLOR",
                        "value": "",
                        "label": "BACKGROUND_COLOR",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": false,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    },
                    {
                        "id": "TEXT_COLOR",
                        "value": "",
                        "label": "TEXT_COLOR",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": false,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    }
                ]
            },
            "rows": [
                {
                    "id": "1",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "1"
                        },
                        {
                            "id": "ICON",
                            "value": "fas fa-stethoscope"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "1"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#0f77e7"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "2",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "2"
                        },
                        {
                            "id": "ICON",
                            "value": "fas fa-tint"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "2"
                        },
                        {
                            "id": "LABEL",
                            "value": "Prise de sang"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#ff534b"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "3",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "3"
                        },
                        {
                            "id": "ICON",
                            "value": "fas fa-plus-square"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "3"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation spéciale"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#40bb4b"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "4",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "4"
                        },
                        {
                            "id": "ICON",
                            "value": "far fa-meh-blank"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "4"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation pré-natale"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#ffa467"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "5",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "5"
                        },
                        {
                            "id": "ICON",
                            "value": "far fa-meh-blank"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "5"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation post-natale"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#e7bc08"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                }
            ],
            "searchRow": [
                {
                    "fields": [
                        {
                            "id": "LABEL",
                            "value": "",
                            "label": "eventType:label",
                            "kind": "TEXTFIELD",
                            "type": "VARCHAR",
                            "isPrimaryKey": false,
                            "optionValues": [],
                            "isMandatory": false,
                            "isVisible": true,
                            "hasStartWildcard": true,
                            "isDisabled": false,
                            "isSortable": false
                        }
                    ]
                }
            ]
        };

        const tList: TList = TList.fromJson(json);

        expect(tList.id).toEqual("EVENT_TYPES_LIGHT");
        expect(tList.baseRow.fields.length).toEqual(6);
        expect(tList.rows.length).toEqual(5);
        expect(tList.rows[0].getFieldById("DISPLAY_ORDER") instanceof TField).toEqual(true);
        expect(tList.rows[1].getFieldById("ICON")?.value).toEqual("fas fa-tint");
    });
});

/*describe("toJson()", () => {
    it('should return a JSON representation of the TList', () => {
        const json = {
            "id": "EVENT_TYPES_LIGHT",
            "baseRow": {
                "fields": [
                    {
                        "id": "ID_EVENT_TYPE",
                        "value": "",
                        "label": "ID_EVENT_TYPE",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": true,
                        "optionValues": [],
                        "isVisible": false,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    },
                    {
                        "id": "ICON",
                        "value": "",
                        "label": "eventType:icon",
                        "kind": "IMAGE",
                        "customDisplay": "FieldEventTypeIcon",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": true,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    },
                    {
                        "id": "DISPLAY_ORDER",
                        "value": "",
                        "label": "eventType:displayOrder",
                        "kind": "TEXTFIELD",
                        "customDisplay": "ORDER",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": true,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": true
                    },
                    {
                        "id": "LABEL",
                        "value": "",
                        "label": "eventType:label",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": true,
                        "hasStartWildcard": true,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": true
                    },
                    {
                        "id": "BACKGROUND_COLOR",
                        "value": "",
                        "label": "BACKGROUND_COLOR",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": false,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    },
                    {
                        "id": "TEXT_COLOR",
                        "value": "",
                        "label": "TEXT_COLOR",
                        "kind": "TEXTFIELD",
                        "customDisplay": "NONE",
                        "type": "VARCHAR",
                        "isPrimaryKey": false,
                        "optionValues": [],
                        "isVisible": false,
                        "hasStartWildcard": false,
                        "defaultValue": "",
                        "isDisabled": false,
                        "isSortable": false
                    }
                ]
            },
            "rows": [
                {
                    "id": "1",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "1"
                        },
                        {
                            "id": "ICON",
                            "value": "fas fa-stethoscope"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "1"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#0f77e7"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "2",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "2"
                        },
                        {
                            "id": "ICON",
                            "value": "fas fa-tint"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "2"
                        },
                        {
                            "id": "LABEL",
                            "value": "Prise de sang"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#ff534b"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "3",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "3"
                        },
                        {
                            "id": "ICON",
                            "value": "fas fa-plus-square"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "3"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation spéciale"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#40bb4b"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "4",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "4"
                        },
                        {
                            "id": "ICON",
                            "value": "far fa-meh-blank"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "4"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation pré-natale"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#ffa467"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                },
                {
                    "id": "5",
                    "fields": [
                        {
                            "id": "ID_EVENT_TYPE",
                            "value": "5"
                        },
                        {
                            "id": "ICON",
                            "value": "far fa-meh-blank"
                        },
                        {
                            "id": "DISPLAY_ORDER",
                            "value": "5"
                        },
                        {
                            "id": "LABEL",
                            "value": "Consultation post-natale"
                        },
                        {
                            "id": "BACKGROUND_COLOR",
                            "value": "#e7bc08"
                        },
                        {
                            "id": "TEXT_COLOR",
                            "value": "#ffffff"
                        }
                    ]
                }
            ]
        };

        const tList = TList.fromJson(json);

        const result = tList.toJson();

        const expected = {
            "id": "EVENT_TYPES_LIGHT",
            "baseRow": {
                "ID_EVENT_TYPE": "",
                "ICON": "",
                "DISPLAY_ORDER": "",
                "LABEL": "",
                "BACKGROUND_COLOR": "",
                "TEXT_COLOR": ""
            },
            "rows": [
                {
                    "ID_EVENT_TYPE": "1",
                    "ICON": "fas fa-stethoscope",
                    "DISPLAY_ORDER": "1",
                    "LABEL": "Consultation",
                    "BACKGROUND_COLOR": "#0f77e7",
                    "TEXT_COLOR": "#ffffff"
                },
                {
                    "ID_EVENT_TYPE": "2",
                    "ICON": "fas fa-tint",
                    "DISPLAY_ORDER": "2",
                    "LABEL": "Prise de sang",
                    "BACKGROUND_COLOR": "#ff534b",
                    "TEXT_COLOR": "#ffffff"
                },
                {
                    "ID_EVENT_TYPE": "3",
                    "ICON": "fas fa-plus-square",
                    "DISPLAY_ORDER": "3",
                    "LABEL": "Consultation spéciale",
                    "BACKGROUND_COLOR": "#40bb4b",
                    "TEXT_COLOR": "#ffffff"
                },
                {
                    "ID_EVENT_TYPE": "4",
                    "ICON": "far fa-meh-blank",
                    "DISPLAY_ORDER": "4",
                    "LABEL": "Consultation pré-natale",
                    "BACKGROUND_COLOR": "#ffa467",
                    "TEXT_COLOR": "#ffffff"
                },
                {
                    "ID_EVENT_TYPE": "5",
                    "ICON": "far fa-meh-blank",
                    "DISPLAY_ORDER": "5",
                    "LABEL": "Consultation post-natale",
                    "BACKGROUND_COLOR": "#e7bc08",
                    "TEXT_COLOR": "#ffffff"
                }
            ]
        };

        const test = JSON.stringify(tList);

        expect(result).toEqual(expected);
    });
});
*/
