import TField from "../TField";

describe("fromJson()", () => {
    it('should return new TSimpleField from JSON object', function () {
        const json = {
            "id": "LABEL",
            "value": "This is the value",
            "label": "namespace:label",
            "kind": "TEXTFIELD",
            "customDisplay": "",
            "type": "VARCHAR",
            "isPrimaryKey": false,
            "optionValues": [{"value": "option value 1", "label": "option label 1"}, {"value": "option value 2", "label": "option label 2"}],
            "isVisible": true,
            "hasStartWildcard": true,
            "defaultValue": "",
            "isDisabled": false,
            "isSortable": true
        };

        const simpleField = TField.fromJson(json);

        expect(simpleField.id).toEqual("LABEL");
        expect(simpleField.value).toEqual("This is the value");
        expect(simpleField.label).toEqual("namespace:label");
        expect(simpleField.kind).toEqual("TEXTFIELD");
        expect(simpleField.customDisplay).toEqual(null);
        expect(simpleField.type).toEqual("VARCHAR");
        expect(simpleField.isPrimaryKey).toEqual(false);
        expect(simpleField.optionValues?.length).toEqual(2);
        expect(simpleField.isVisible).toEqual(true);
        expect(simpleField.hasStartWildcard).toEqual(true);
        expect(simpleField.defaultValue).toEqual("");
        expect(simpleField.isDisabled).toEqual(false);
        expect(simpleField.isSortable).toEqual(true);
    });
})
