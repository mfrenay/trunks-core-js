import TRow from "../TRow";
import TField from "../TField";

describe("fromJson()", () => {
    it('should return new TRow from JSON object', () => {
        const json = {
            "nr": 101,
            "fields": [
                {
                    "id": "ID_EVENT_TYPE",
                    "value": "2"
                },
                {
                    "id": "ICON",
                    "value": "fas fa-tint"
                },
                {
                    "id": "DISPLAY_ORDER",
                    "value": "2"
                },
                {
                    "id": "LABEL",
                    "value": "Prise de sang"
                },
                {
                    "id": "BACKGROUND_COLOR",
                    "value": "#ff534b"
                },
                {
                    "id": "TEXT_COLOR",
                    "value": "#ffffff"
                }
            ]
        };

        const tRow = TRow.fromJson(json);

        expect(tRow.nr).toEqual(101);
        expect(tRow.fields.length).toEqual(6);
        expect(tRow.fields[0] instanceof TField).toEqual(true);
    });
});
