import TCForm from "../TCForm";

describe("TCForm.fromJson", () => {
  it("should return a TCForm", () => {
    const json = {
      "id": "C_USERS",
      "forms": [
        {
          "id": "USERS",
          "state": "UNCHANGED",
          "row": {
            "id": "",
            "fields": [
              {
                "id": "ID_USER",
                "value": "3"
              },
              {
                "id": "FIRSTNAME",
                "value": "Laurent"
              },
              {
                "id": "LASTNAME",
                "value": "Outan"
              },
              {
                "id": "SLUG",
                "value": "outan-laurent"
              }
            ]
          }
        }
      ],
      "lists": [
        {
          "id": "MEDICAL_CENTERS",
          "customObjectParams": {
            "YN_BASE_ROW": "N",
            "SIMPLE": "Y"
          },
          "rows": [
            {
              "id": "3",
              "fields": [
                {
                  "id": "ID_USER",
                  "value": "3"
                },
                {
                  "id": "ID_MEDICAL_CENTER",
                  "value": "3"
                },
                {
                  "id": "NAME",
                  "value": "CHU"
                },
                {
                  "id": "PHONE",
                  "value": "04 254 25 23"
                },
                {
                  "id": "ADDRESS",
                  "value": "Rue des lapins bleus 48a, 4600 Hermalle"
                }
              ],
              "lists": [
                {
                  "id": "EVENT_TYPES",
                  "rows": [
                    {
                      "id": "1",
                      "fields": [
                        {
                          "id": "ID_EVENT_TYPE",
                          "value": "1"
                        },
                        {
                          "id": "ID_USER",
                          "value": "3"
                        },
                        {
                          "id": "ID_MEDICAL_CENTER",
                          "value": "3"
                        },
                        {
                          "id": "TITLE",
                          "value": "Consultation"
                        }
                      ]
                    },
                    {
                      "id": "2",
                      "fields": [
                        {
                          "id": "ID_EVENT_TYPE",
                          "value": "2"
                        },
                        {
                          "id": "ID_USER",
                          "value": "3"
                        },
                        {
                          "id": "ID_MEDICAL_CENTER",
                          "value": "3"
                        },
                        {
                          "id": "TITLE",
                          "value": "Prise de sang"
                        }
                      ]
                    },
                    {
                      "id": "3",
                      "fields": [
                        {
                          "id": "ID_EVENT_TYPE",
                          "value": "3"
                        },
                        {
                          "id": "ID_USER",
                          "value": "3"
                        },
                        {
                          "id": "ID_MEDICAL_CENTER",
                          "value": "3"
                        },
                        {
                          "id": "TITLE",
                          "value": "Urgence"
                        }
                      ]
                    },
                    {
                      "id": "4",
                      "fields": [
                        {
                          "id": "ID_EVENT_TYPE",
                          "value": "4"
                        },
                        {
                          "id": "ID_USER",
                          "value": "3"
                        },
                        {
                          "id": "ID_MEDICAL_CENTER",
                          "value": "3"
                        },
                        {
                          "id": "TITLE",
                          "value": "Consultation pré-natale"
                        }
                      ]
                    },
                    {
                      "id": "5",
                      "fields": [
                        {
                          "id": "ID_EVENT_TYPE",
                          "value": "5"
                        },
                        {
                          "id": "ID_USER",
                          "value": "3"
                        },
                        {
                          "id": "ID_MEDICAL_CENTER",
                          "value": "3"
                        },
                        {
                          "id": "TITLE",
                          "value": "Consultation post-natale"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ],
          "nbColCriteria": 2,
          "maxRowToRetrieve": 0,
          "limitMaxRowToRetrieve": 0,
          "isMaxRowToRetrieveReached": false,
          "pageSize": 0,
          "isDBSort": false,
          "sortingOrder": 1,
          "hasAddFooter": false
        }
      ]
    };

    const tCForm = TCForm.fromJson(json);

    expect(tCForm.forms.length).toEqual(1);
    expect(tCForm.lists.length).toEqual(1);
    expect(tCForm.lists[0].rows.length).toEqual(1);
  });
});
