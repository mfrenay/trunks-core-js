import TGroupField from "../TGroupField";
import TRow from "../TRow";

describe("TGroupField with TRow value", () => {
    describe("fromJson()", () => {
        it('should return new TGroupField from JSON object', function () {
            // const json = {
            //     "patient": {
            //         "firstName": "John",
            //         "lastName": "Doe",
            //         "birthDate": "1995-05-10",
            //     }
            // };

            const json = {
                "patient": {
                    "id": "row",
                    "fields": [
                        {"id": "firstName", "value": "John"},
                        {"id": "lastName", "value": "Doe"},
                        {"id": "birthDate", "value": "1995-05-10"},
                    ]
                }
            };

            const tGroupField = TGroupField.fromJson(json);

            // TGroupField has the correct ID
            expect(tGroupField.id).toEqual("patient");

            // TGroupField value is a TRow with 3 fields
            expect(tGroupField.value instanceof TRow).toEqual(true);
            expect(tGroupField.value!.fields.length).toEqual(3);
            expect(tGroupField.value!.getRowFieldById("lastName")?.value).toEqual("Doe");
        });
    });
});

describe("TGroupField with null properties", () => {
    describe("fromJson() on empty object", () => {
        it('should throw an error', function () {
            const json = { };

            expect(() => TGroupField.fromJson(json)).toThrowError("TGroupField must have one and only one child");
        });
    });

    describe("fromJson() with null TRow value", () => {
        it('should throw an error', function () {
            const json = {
                "patient": null
            };

            expect(() => TGroupField.fromJson(json)).toThrowError();
        });
    });
});


