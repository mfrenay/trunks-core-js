import TOptionValue from "../TOptionValue";

describe("fromJson()", () => {
    it('should return new TOptionValue from JSON object', () => {
        const json = {
            "value": "option value",
            "label": "option label"
        };

        const tOptionValue = TOptionValue.fromJson(json);

        expect(tOptionValue.value).toEqual("option value");
        expect(tOptionValue.label).toEqual("option label");
    });
})
