import TAbstractField from "./TAbstractField";
import TRow from "./TRow";

/**
 * Type of the value in a TGroupField
 */
export type TGroupFieldValue = TRow;

/**
 * GroupField
 *
 * Parent of a set of fields (SimpleField)
 * SimpleField can be grouped to create a better structure of a form
 */
class TGroupField extends TAbstractField<TGroupFieldValue | null> {

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(id: string, value: TRow | null) {
        super(id, value);
    }

    static fromJson(json: any): TGroupField {
        const keys = Object.keys(json);
        if (keys.length !== 1) {
            throw new Error("TGroupField must have one and only one child");
        }

        const id = keys[0];
        return new TGroupField(id, TRow.fromJson(json[id]));
    }
}

export default TGroupField;
