import TAbstractField from "./TAbstractField";

/**
 * Type of the value in a TSimpleField
 */
export type TSimpleFieldValue = string | boolean;

/**
 * TSimpleField
 *
 * Represents a field which can be a TEXTFIELD, LABEL, SELECT, ...
 * Its value is either:
 *  - a simple string value (can be a text, number, date, etc. in a string format)
 *  - a boolean value
 */
class TSimpleField extends TAbstractField<TSimpleFieldValue | null> {

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(id: string, value: TSimpleFieldValue | null) {
        super(id, value);
    }

    /**
     * Returns a JSON object representing the TField
     * This method is automatically called when used with JSON.stringify(...)
     */
    toJSON(): object {
        return {
            "id": this.id,
            "value": this.value
        };
    }
}

export default TSimpleField;
