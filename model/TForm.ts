import TRow, {TRowField} from "./TRow";
import TField, {TFieldValue} from "./TField";
import TGroupField from "./TGroupField";
import Logger from "../logger/logger";
import TRowState from "./TRowState";
import TWebForm from "./TWebForm";

/**
 * TForm
 *
 * Represents a web form defined by an XML data source on the web server.
 * A TForm, which can be a new (empty values-) TForm is displayed on the web and its fields are displayed as defined by the data source.
 * The data of this form are retrieved from the server by calling a generic web service : DataWebService.getFormData()
 * The data of this form are saved to the server by calling a generic web service : DataWebService.saveFormData()
 */
class TForm extends TWebForm {
    /**
     * Base row of the form
     */
    baseRow: TRow;
    /**
     * List of fields composing the form
     */
    row: TRow;
    /**
     * State of form (new or existing form)
     */
    rowState: TRowState;

    constructor(id: string, baseRow: TRow = new TRow(), row: TRow = new TRow()) {
        super(id);
        this.baseRow = baseRow;
        this.row = row;
        this.rowState = TRowState.UNCHANGED;
    }

    /**
     * Create a TForm from a Json object
     *
     * @param json
     */
    static fromJson(json: any): TForm {
        const tForm = new TForm(
            json.id,
            json.baseRow ? TRow.fromJson(json.baseRow) : undefined,
            json.row ? TRow.fromJson(json.row) : undefined,
        );
        tForm.rowState = json.state;
        return tForm;
    }

    /**
     * Returns a field (SimpleField, GroupField) by its id
     * @param id ID of the field
     */
    getRowFieldById(id: string): TRowField {
        const arrayFieldId: string[] = id.split(".");
        let tField: TField | null = null;

        if (arrayFieldId.length === 1) {
            tField = this.row!.getFieldById(arrayFieldId[0]);
        }
        else {
            const groupField = this.row!.getRowFieldById(arrayFieldId[0]) as TGroupField;

            if (groupField != null && groupField.value != null)
                tField = groupField.value.getFieldById(arrayFieldId[1]);
        }

        if (tField == null) {
            Logger.e(`Field with ID '${id}' was not found`)
            throw new Error(`Field with ID '${id}' was not found`);
        }

        return tField;
    }

    getFieldValueById(id: string): TFieldValue | TRow | null {
        return this.getRowFieldById(id).value;
    }

    getFieldById(id: string): TField {
        return this.getRowFieldById(id) as TField;
    }

    setFieldValueById(id: string, value: TFieldValue | TRow | null) {
        this.getRowFieldById(id).value = value;
    }

    /**
     * @returns true if at least one form value has changed, false otherwise.
     */
    hasChanged(): boolean {
        return this.row.fields.find((field) => field.hasChanged()) != null;
    }
}

export default TForm;
