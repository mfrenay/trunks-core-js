/**
 * TAbstractField
 *
 * Master class of all kind of Field (SimpleField, GroupField)
 * Each Field must contain an identifier : id and a value.
 * The type of the value is generic (type of T).
 */

/**
 * Interface defining the required properties that must have a TField (SimpleField, GroupField, ...)
 */
export interface IField<T> {
    id: string;
    value: T;
    readonly initialValue: T;

    hasChanged: () => boolean;
}

abstract class TAbstractField<T> implements IField<T> {
    id: string;
    value: T;
    readonly initialValue: T;

     protected constructor(id: string, value: T) {
        this.id = id;
        this.value = value;
        this.initialValue = value;
    }

    /**
     * Return true if the field has changed, false otherwise
     */
    hasChanged(): boolean {
        return this.value !== this.initialValue;
    }
}

export default TAbstractField;
