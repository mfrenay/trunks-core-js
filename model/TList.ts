import TRow from "./TRow";
import TField from "./TField";
import StringUtils from "../utilities/StringUtils";
import Logger from "../logger/logger";
import TWebForm from "./TWebForm";

// class TSearchList {
//     private tList: TList;
//     private searchFields: TSearchField[];
// }
export enum TListCustomObjectParamsId {
    CONTEXT_FIELD = "CONTEXT_FIELD",
    DEFAULT_FILTERS_OPEN = "DEFAULT_FILTERS_OPEN",
    DEFAULT_SORTING_FIELD = "DEFAULT_SORTING_FIELD",
    SHOW_NB_ROWS = "SHOW_NB_ROWS",
    EXPORT_XLS = "EXPORT_XLS",
    EXPORT_XLS_LIST = "EXPORT_XLS_LIST",
    IMPORT_XLS = "IMPORT_XLS",
    INSERT_ROW_ON_ADD = "INSERT_ROW_ON_ADD",
    ROW_CLASS_NAME_FIELD = "ROW.CLASS_NAME_FIELD",
    ONLY_SEND_ROWS_ON_FILTER = "ONLY_SEND_ROWS_ON_FILTER",
    SUB_LIST = "SUB_LIST",
    SUM = "SUM",
    TITLE = "TITLE",
}

type TListCustomObjectParams = {
    [key in TListCustomObjectParamsId]?: string;
};


class TList extends TWebForm {
    baseRow: TRow;
    searchRow: TRow;
    rows: TRow[];

    /**
     * Indication if the number of maximum rows has been reached on the server.
     * The server limits the retrieved rows
     */
    isMaxRowToRetrieveReached : boolean;
    /**
     * The number of rows that could be retrieved for this TList data source
     */
    maxRowToRetrieve: number;
    /**
     * The limit of rows that can be retrieved for this TList data source by request
     */
    limitMaxRowToRetrieve: number;

    /**
     * The ID of the field on which the datatable is currently sorted.
     */
    sortingFieldId: string | null;
    /**
     * A number indicating the sorting order (ascending or descending) currently displayed by the datatable.
     * 1 for ASC order, -1 for DESC order
     */
    sortingOrder: number;
    /**
     * Custom object Parameters
     */
    customObjectParams: TListCustomObjectParams | null;
    /**
     * Number of columns to display for the search criteria
     */
    nbColCriteria: number;
    /**
     * Number of rows to display per page
     */
    pageSize: number;
    /**
     * Sorting list must be done by the back-end or not
     */
    isDBSort: boolean;
    /**
     * Display an add footer under the list
     */
    hasAddFooter: boolean;

    /**
     * Constructor
     * @param id
     * @param baseRow
     * @param rows
     * @param searchRow
     */
    constructor(id: string, baseRow: TRow = new TRow(), rows: TRow[] = [], searchRow: TRow = new TRow()) {
        super(id);
        this.baseRow = baseRow;
        this.searchRow = searchRow;
        this.rows = rows;

        this.isMaxRowToRetrieveReached = false;
        this.maxRowToRetrieve = 0;
        this.limitMaxRowToRetrieve = 0;

        this.sortingFieldId = null;
        this.sortingOrder = -1; // Setup with -1, like this on the first sort event, it will be switched to 1 that will make the sorting ASC
        this.customObjectParams = null;
        this.nbColCriteria = 4;

        this.pageSize = 10;

        this.isDBSort = false;
        this.hasAddFooter = false;
    }

    /**
     * Create a TList from a JSON object
     *
     * @param json
     */
    static fromJson(json: any): TList {
        const tList = new TList(
            json.id,
            json.baseRow ? TRow.fromJson(json.baseRow) : undefined,
            json.rows.map((row: any) => TRow.fromJson(row)),
            json.searchRow ? TRow.fromJson(json.searchRow) : undefined
        );

        tList.customObjectParams = json.customObjectParams ?? {};

        tList.isMaxRowToRetrieveReached = json.isMaxRowToRetrieveReached;
        tList.maxRowToRetrieve = json.maxRowToRetrieve;
        tList.limitMaxRowToRetrieve = json.limitMaxRowToRetrieve;
        tList.nbColCriteria = json.nbColCriteria;

        if (json.pageSize != null && json.pageSize > 1) {
            tList.pageSize = json.pageSize;
        }

        tList.isDBSort = json.isDBSort;
        tList.sortingFieldId = json.sortingFieldId;
        tList.sortingOrder = json.sortingOrder;
        tList.hasAddFooter = json.hasAddFooter;

        return tList;
    }

    /**
     * Sort the TList rows based on a given fields
     *
     * @param field The field of the list on which make the sort
     * @param newSortingOrder The sorting order (1 for ASC order, -1 for DESC order)
     */
    sort(field: TField, newSortingOrder?: number | null): void {
        Logger.d("<<<<<<<<< TList.sort - field.id = " + field.id + " - newSortingOrder = " + newSortingOrder);
        // Switch the sorting order only when the user clicks on the same column,
        // If the user clicks on another column, keep the same sorting order.
        //const newSortingOrder = this.sortingFieldId === field.id ? this.sortingOrder * -1 : 1;
        if (newSortingOrder == null) {
            newSortingOrder = 1;
        }

        // Don't change the sorting info in the tList, only in the local state
        // Like this, when the tList is reloaded through the advanced search, the
        this.sortingFieldId = field.id;
        this.sortingOrder = newSortingOrder;

        const sortingOrder = newSortingOrder;

        this.rows.sort((row1: TRow, row2: TRow): number => {
            const fieldType = StringUtils.getNString(this.baseRow.getFieldById(field.id)?.type);
            const v1 = row1.getFieldById(field.id)!.typedValue(fieldType);
            const v2 = row2.getFieldById(field.id)!.typedValue(fieldType);

            // Put the empty strings at the end of the list
            if (typeof v1 === "string" && (v1 === "") && typeof v2 === "string" && (v2 === "")) {
                return 0;
            }
            else if (typeof v1 === "string" && (v1 === "")) {
                return sortingOrder;
            }
            else if (typeof v2 === "string" && (v2 === "")) {
                return sortingOrder * -1;
            }

            return sortingOrder * (v1 < v2 ? -1 : (v1 > v2 ? 1 : 0));
        });
    }

    static clone(tList: TList) {
        // FIXME: make a deep clone
        const cloneTList = new TList(tList.id, tList.baseRow, tList.rows, tList.searchRow);
        Object.assign(cloneTList, tList);

        return cloneTList;
    }
}

export default TList;
