import TOptionValue from "./TOptionValue";
import TAbstractField from "./TAbstractField";

import TypeUtils from "../utilities/TypeUtils";
import StringUtils from "../utilities/StringUtils";

import {TFunction} from "i18next";
import {config} from "../config";
import TFieldKind from "./TFieldKind";
import TRow from "./TRow";
import TForm from "./TForm";
import NumberUtils from "../utilities/NumberUtils";
import {translate} from "../utilities/TranslationUtils";

export enum TFieldState {
    DISABLED = "#DISABLED#",
}
export enum TFieldAction {
    OPEN_LIST = "OPEN_LIST",
    OPEN_LIST_POPUP = "OPEN_LIST_POPUP",
    OPEN_FORM = "OPEN_FORM",
    EDIT_FORM = "EDIT_FORM",
}

enum DisplayType {
    COLOR_PICKER = "COLOR_PICKER",
    ICON_PICKER = "ICON_PICKER",
    MULTI_CHECKBOX = "MULTI_CHECKBOX",
    TIME = "TIME",
    SELECT_AUTO_COMPLETE = "SELECT_AUTO_COMPLETE"
}

export enum TFieldCustomObjectParamsId {
    BLANK_OPTION_LABEL = "BLANK_OPTION_LABEL",
    DATA_SUFFIX = "DATA_SUFFIX",
    DISPLAY_LABEL = "DISPLAY_LABEL",
    FONTAWESOME = "FONTAWESOME",
    LINK_ACTION = "LINK_ACTION",
    MAX = "MAX",
    MIN = "MIN",
    POPUP_TITLE = "POPUP_TITLE",
    PRECISION = "PRECISION",
    ROUTE_PATH = "ROUTE_PATH",
    STEP = "STEP",
    SUM = "SUM",
    URL_IMAGE = "URL_IMAGE",
    TRANSLATION_CONTEXT = "TRANSLATION_CONTEXT",
    TRANSLATE = "TRANSLATE",
    ON_CHANGE_ACTION = "ON_CHANGE_ACTION",
}

type TFieldCustomObjectParams = {
    [key in TFieldCustomObjectParamsId]?: string;
};

/**
 * Type of the value in a TSimpleField
 */
export type TFieldValue = string | boolean;

/**
 * TField
 *
 * Represents a field which can be a TEXTFIELD, LABEL, SELECT, ...
 * Its value is either:
 *  - a simple string value (can be a text, number, date, etc. in a string format)
 *  - a boolean value
 */
class TField extends TAbstractField<TFieldValue | null> {

    /**
     * Label displayed in a form or as column grid for this field
     */
    label: string | null;
    /**
     * Kind of component for the field (TEXTFIELD, LABEL, SELECT, ...)
     */
    kind: TFieldKind | null;
    /**
     * Data type of the field (VARCHAR, INTEGER, NUMBER, DATE, ...)
     */
    type: string | null;
    /**
     * If true, the field needs to have a value, otherwise the value can be empty.
     */
    isMandatory: boolean;
    /**
     * List of values for a SELECT field
     */
    optionValues: TOptionValue[] | null;
    /**
     * Should the field be display in the form or in columm in a grid
     */
    isVisible: boolean;
    /**
     * Should the field be display in the add row in the grid
     */
    isVisibleAdd: boolean;
    /**
     * Determine if the field is a primary key
     */
    isPrimaryKey: boolean;
    /**
     * Indicates if the matching must start with a wildcard.
     * When true, the matching is of 'contains' type.
     * When false, the matching is of 'starting with' type.
     */
    hasStartWildcard: boolean;
    /**
     * This property indicates that the field must be rendered with a special component, ColorPicker for example.
     */
    customDisplay: DisplayType | null;
    /**
     * Simple string value (text, number, date, etc.)
     */
    defaultValue: TFieldValue | null;
    /**
     * Indicated if the field must be disabled
     */
    isDisabled: boolean;
    /**
     * Indicates if the field can be sorted. True if it is sortable, false otherwise.
     */
    isSortable: boolean;
    /**
     * Format
     */
    format: string | null;
    /**
     * Action for BUTTON, IMAGE or LINK field
     */
    action: string | null;
    /**
     * Regular expression that the field value must match
     */
    regexp: string | null;
    /**
     * Rules to force label
     * e.g.
     * forceLabelIf="#PROFILE#=USER,VISU"
     * forceLabelIf="#STATE#!=NEW"
     */
    forceLabelIf: string | null;
    /**
     * Custom object Parameters
     */
    customObjectParams: TFieldCustomObjectParams | null;

    constructor (
        id: string,
        value: TFieldValue | null = null,
        label: string | null = null,
        kind: TFieldKind | null = null,
        optionValues: TOptionValue[] | null = null,
        isVisible: boolean = true,
        type: string | null = null,
        isPrimaryKey: boolean = false,
        customDisplay: DisplayType | null = null,
        isSortable: boolean = true
    ) {
        super(id, value);

        this.label = label;
        this.kind = kind;
        this.isMandatory = false;
        this.optionValues = optionValues;
        this.isVisible = isVisible;
        this.isVisibleAdd = false;
        this.type = type;
        this.isPrimaryKey = isPrimaryKey;
        this.hasStartWildcard = false;
        this.customDisplay = customDisplay;
        this.defaultValue = null;
        this.isDisabled = false;
        this.isSortable = isSortable;
        this.format = null;
        this.action = null;
        this.regexp = null;
        this.forceLabelIf = null;
        this.customObjectParams = null;
    }

    /**
     * Create a TField from a JSON object
     *
     * @param json
     */
    static fromJson(json: any): TField {
        const simpleField = new TField(
            json.id,
            json.value,
            json.label,
            json.kind,
            json.optionValues?.map((optionJson: any) => TOptionValue.fromJson(optionJson)),
            json.isVisible,
            json.type,
            json.isPrimaryKey,
            json.customDisplay && json.customDisplay !== "" ?  (
                    Object.values(DisplayType).includes(json.customDisplay) ? json.customDisplay : config.componentFactory[json.customDisplay]
                ): null,
            json.isSortable
        );

        simpleField.isVisibleAdd = json.isVisibleAdd;
        simpleField.hasStartWildcard = json.hasStartWildcard;
        simpleField.isDisabled = json.isDisabled;
        simpleField.isMandatory = json.isMandatory;
        simpleField.setDefaultValue(json.defaultValue);
        simpleField.format = json.format;
        simpleField.action = json.action;
        simpleField.regexp = json.regexp
        simpleField.forceLabelIf = json.forceLabelIf
        simpleField.customObjectParams = json.customObjectParams;

        return simpleField;
    }

    /**
     * Returns a JSON object representing the TField
     * This method is automatically called when used with JSON.stringify(...)
     */
    toJSON(): object {
        return {
            "id": this.id,
            "value": this.value
        };
    }

    /**
     * Returns the formatted value of the field.
     * Essentially used to be displayed to web form.
     * - Returns empty string for null value
     * - Returns a formatted date DD/MM/YYYY
     */
    getFormattedValue(baseField: TField, t: TFunction | null = null, options?: {language: string}): string {
        if (StringUtils.getNString(this.value) === "" || this.value == null) {
            // If the value is null or empty, return a dash "-" character
            return "-";
        }

        if (TypeUtils.isBoolean(this.value)) {
            return this.value ? "X" : "";
        }

        if (baseField.type === "INTEGER") {
            return NumberUtils.roundDecimal(parseInt(this.value), 0)
              .toLocaleString(options?.language);
        }

        if (baseField.type === "NUMBER") {
            const precision = (baseField.customObjectParams?.PRECISION && parseInt(baseField.customObjectParams.PRECISION)) || 2;
            return NumberUtils.roundDecimal(parseFloat(this.value), precision)
                .toLocaleString(options?.language, {
                    minimumFractionDigits: precision,
                    maximumFractionDigits: precision
                });
        }

        if (baseField.type === "DATE") {
            const date = new Date(this.value);
            let formatOptions: Intl.DateTimeFormatOptions = {};

            if (!baseField.format || baseField.format === "yyyy-MM-dd") {
                // If there is no format defined for the field
                // or if the defined format is "yyyy-MM-dd",
                // only display the date part
                formatOptions = {...formatOptions, dateStyle: "short", timeStyle: undefined};
            } else {
                // Otherwise, display the full date and time in a short format
                formatOptions = {...formatOptions, dateStyle: "short", timeStyle: "short"};
            }

            return date.toLocaleString(options?.language, formatOptions);
        }

        if (baseField.optionValues != null && baseField.optionValues.length > 0) {
            const option = baseField.optionValues.find(option => option.value === this.value);
            if (option != null) {
                return translate(option.label, baseField, t);
            }
        }

        return translate(this.value, baseField, t);
    }

    /**
     * Returns the typed value of the field.
     * Essentially used to be compared when sorting a datatable.
     * - Returns empty string for null value
     * - Returns a date, number or string
     */
    typedValue(fieldType: string | null): string | number | Date {
        if (StringUtils.getNString(fieldType) === "") {
            fieldType = this.type;
        }

        if (StringUtils.getNString(this.value) === "") {
            return "";
        }

        if (TypeUtils.isBoolean(this.value)) {
            return this.value ? "X" : "";
        }

        if (fieldType === "DATE") {
            return new Date(this.value!);
        }

        if (fieldType === "NUMBER" || fieldType === "INTEGER") {
            const number = Number(this.value!);
            if (isNaN(number)) {
                return 0;
            }
            return number;
        }

        return this.value!;
    }

    /**
     * Returns the formatted value of the field.
     * Essentially used to be displayed to web form.
     * - Returns empty string for null value
     * - Returns a formatted date according to format (maybe from server side)
     */
    get stringValue(): string {
        if (this.value == null) {
            return "";
        }

        if (TypeUtils.isBoolean(this.value)) {
            return this.value ? "X" : "";
        }

        return this.value;
    }

    /**
     * Returns the formatted initial value of the field.
     * Essentially used to be displayed to web form.
     * - Returns empty string for null value
     * - Returns a formatted date according to format (maybe from server side)
     */
    get stringInitialValue(): string {
        if (this.initialValue == null) {
            return "";
        }

        if (TypeUtils.isBoolean(this.initialValue)) {
            return this.initialValue ? "X" : "";
        }

        return this.initialValue;
    }

    /**
     * booleanValue
     */
    get booleanValue(): boolean {
        return TypeUtils.getBooleanValue(this.value);
    }

    /**
     * setDefaultValue
     * @param value
     */
    setDefaultValue(value: TFieldValue | null) {
        if (this.value == null) {
            this.value = value;
        }

        this.defaultValue = value;
    }

    /**
     *
     * @param criteriaField
     * @param tRow
     * @param tForm
     * @return
     */
    getCriteriaFieldValue (criteriaField: string, tRow:TRow, tForm: TForm /*, userSession:UserSession*/): string {
        let criteriaFieldValue = "";
        // special criteria
        if (criteriaField === "#STATE#") {
            criteriaFieldValue = tForm.rowState;
        }
        // else if(criteriaField === "#PROFILE#"){
        //     criteriaFieldValue = StringUtils.getNString(userSession.profile);
        // }
        // else if(criteriaField === "#ID_USER#") {
        //     criteriaFieldValue = StringUtils.getNString(userSession.idUser);
        // }
        // else if(criteriaField === "#LANGUAGE#") {
        //     criteriaFieldValue = StringUtils.getNString(userSession.language);
        // }
        else { // field
            const fieldCriteria = tRow.getFieldById(criteriaField);
            if (fieldCriteria != null) {
                criteriaFieldValue = fieldCriteria.stringValue;
            }
        }

        return criteriaFieldValue;
    }

    /**
     * Evaluate rules to eventually force label rather than TEXTFIELD, SELECT, ...
     * @param baseField
     * @param tRow
     * @param tForm
     */
    isForceLabel(baseField: TField, tRow: TRow, tForm: TForm /*, userSession:UserSession*/):boolean {
        //Logger.d("<<<<<<<< TField.isForceLabel - forceLabelIf("+baseField.forceLabelIf+")");
        //Logger.d("<<<<<<<< TField.isForceLabel - rowState("+tForm.rowState+")");
        // mich - 06/04/2021
        if (baseField != null && StringUtils.getNString(baseField.forceLabelIf) !== "") {
            /*
             * FORCE_LABEL_IF="YN_HEALTH_PROBLEM=Y"
             * FORCE_LABEL_IF="#PROFILE#=USER,VISU"
             * FORCE_LABEL_IF="#STATE#!=NEW"
             * FORCE_LABEL_IF="#STATE#!=NEW;#ID_USER#=3"
             */
            // parts
            const parts = baseField.forceLabelIf!.split(";");
            for (let k = 0; k < parts.length; k++) {
                // is criteria equals or not equals ?
                const criteriaExpression = parts[k];
                let criteriaParts: string[] | null = null;
                let isCriteriaEquals = true;

                if (criteriaExpression.indexOf("!=") !== -1) { // "not equals" criteria
                    criteriaParts = criteriaExpression.split("!=");
                    isCriteriaEquals = false;
                }
                else { // "equals" criteria
                    criteriaParts = criteriaExpression.split("=");
                }

                if (criteriaParts.length === 2) {
                    const criteriaField = criteriaParts[0];
                    const criteriaFieldValue = this.getCriteriaFieldValue(criteriaField, tRow, tForm);

                    if (criteriaFieldValue !== "" && criteriaParts[1] !== "") {
                        const valuesToCompare = criteriaParts[1].split(",");
                        let nbMatchingValues = 0;

                        for (let j = 0; j < valuesToCompare.length; j++) {
                            let valueToCompare = valuesToCompare[j];
                            // special values
                            /* TODO if(valueToCompare === "#ID_USER#")
                                valueToCompare = StringUtilities.getNString(this.getID_User());*/
                            if (criteriaFieldValue === valueToCompare) {
                                nbMatchingValues++;
                            }
                        }

                        if (!isCriteriaEquals) { // "not equals" criteria
                            // No matching values
                            if (nbMatchingValues === 0) {
                                // force label
                                return true;
                            }
                        }
                        else { // equals" criteria

                            // We found one matching value
                            if (nbMatchingValues > 0) {
                                // force label
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }
}

export default TField;
export {DisplayType};
