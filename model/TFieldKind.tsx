export enum TFieldKind {
    NONE = "",
    TEXTFIELD = "TEXTFIELD",
    TEXTAREA = "TEXTAREA",
    CHECKBOX = "CHECKBOX",
    TOGGLE = "TOGGLE",
    LABEL = "LABEL",
    LIST_VAL = "LIST_VAL",
    SELECT = "SELECT",
    BUTTON = "BUTTON",
    IMAGE = "IMAGE",
}

export default TFieldKind;