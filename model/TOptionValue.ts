import TOptionAttribute, {TOptionAttributeID} from "./TOptionAttribute";
import StringUtils from "../utilities/StringUtils";

class TOptionValue {
    value: string;
    label: string;
    optionGroup?: string;
    attributes: TOptionAttribute[];
    disabled: boolean;

    constructor(value: string, label: string, optionGroup: string | undefined = undefined, attributes: TOptionAttribute[] = []) {
        this.value = value;
        this.label = label;
        this.optionGroup = optionGroup;
        this.attributes = attributes;
        this.disabled = StringUtils.stringToBoolean(this.getAttributeById(TOptionAttributeID.OPTION_DISABLED));
    }

    static fromJson(json: any): TOptionValue {
        const optionValue = new TOptionValue(json.value, json.label, json.optionGroup);
        if (json.attributes)
            optionValue.attributes = json.attributes.map(TOptionAttribute.fromJson);

        optionValue.disabled = StringUtils.stringToBoolean(optionValue.getAttributeById(TOptionAttributeID.OPTION_DISABLED));
        return optionValue;
    }

    /**
     * Find the attribute with the given ID and returns its value
     *
     * @param attributeId The attribute ID for which return
     */
    getAttributeById(attributeId: String): string | null {
        return this.attributes.find((attribute) => (
            attribute.id === attributeId
        ))?.value ?? null;
    }

    /**
     *
     */
    get classNameAttribute(): string | null {
        return this.getAttributeById(TOptionAttributeID.CLASS_NAME);
    }
}

export default TOptionValue;
