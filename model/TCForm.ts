import TForm from "./TForm";
import TList from "./TList";

interface ITCForm {
  id: string;
  forms: TForm[];
  lists: TList[];
}

/**
 * TCForm
 *
 * Represents a web form defined by an XML data source on the web server.
 * A TForm, which can be a new (empty values-) TForm is displayed on the web and its fields are displayed as defined by the data source.
 * The data of this form are retrieved from the server by calling a generic web service : DataWebService.getFormData()
 * The data of this form are saved to the server by calling a generic web service : DataWebService.saveFormData()
 */
class TCForm {
  /**
   * ID of the CForm
   */
  id: string;
  /**
   * forms
   */
  forms: TForm[];
  /**
   * list
   */
  lists: TList[];

  constructor({id, forms, lists}: ITCForm) {
    this.id = id;
    this.forms = forms;
    this.lists = lists;
  }

  static fromJson(json: any): TCForm {
    return new TCForm({
        id: json.id,
        forms: json.forms ? json.forms.map((form: any) => TForm.fromJson(form)) : [],
        lists: json.lists ? json.lists.map((list: any) => TList.fromJson(list)) : [],
    });
  }
}

export default TCForm;
