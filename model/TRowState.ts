export enum TRowState {
    NEW = "NEW",
    MODIFIED = "MODIFIED",
    UNCHANGED = "UNCHANGED",
    DELETED = "DELETED",
}

export default TRowState;