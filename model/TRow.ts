import TField, {TFieldValue} from "./TField";
import {TGroupFieldValue} from "./TGroupField";
import Logger from "../logger/logger";
import {IField} from "./TAbstractField";
import {TSimpleFieldValue} from "./TSimpleField";
import TList from "./TList";

/**
 * Type of a field in a TRow.
 *
 * A TRow contains a list of fields that can be either:
 * - TSimpleField: in this case the field type is IField<TSimpleFieldValue | null>
 * - TGroupField: in this case the field type is IField<TGroupFieldValue | null>
 */
export type TRowField = IField<TSimpleFieldValue | TFieldValue | TGroupFieldValue | null>;

class TRow {
    // private _uuid: string; // Server side ID
    nr: number;
    fields: TRowField[];
    lists: TList[];

    constructor(fields: TRowField[] = [], lists: TList[] = [], nr?: number) {
        this.nr = Number(nr) | 0;
        this.fields = fields;
        this.lists = lists;
    }

    /**
     * Returns a JSON object representing the TField
     * This method is automatically called when used with JSON.stringify(...)
     *
     * (nr is not included because the backend DTO does not have this field)
     */
    toJSON() {
        const {nr, ...other} = this;

        return other;
    }

    /**
     * Create a TRow from a JSON object
     *
     * @param json
     */
    static fromJson(json: any): TRow {
        return new TRow(
            json.fields ? json.fields.map((field: any) => TField.fromJson(field)) : [],
            json.lists ? json.lists.map((list: any) => TList.fromJson(list)) : [],
            json.nr
        );
    }

    getRowFieldById(id: string): TRowField | null {
        const field = this.fields.find(field => field.id === id);

        return field ?? null;
    }

    getFieldById(id: string): TField | null {
        return this.getRowFieldById(id) as TField;
    }

    /**
     * Deprecated (use getPrimaryKeyFields instead)
     *
     * // TODO: remove function
     * @param baseRow
     */
    getPrimaryKeyValue(baseRow: TRow): string {
        return this.getPrimaryKeyField(baseRow).stringValue;
    }

    /**
     * Deprecated (use getPrimaryKeyFields instead)
     *
     * // TODO: remove function
     * @param baseRow
     */
    getPrimaryKeyField(baseRow: TRow): TField {
        const baseField: TField | undefined = baseRow.fields.find(field => field instanceof TField && field.isPrimaryKey) as TField;

        if (baseField == null) {
            Logger.e("No primary key field detected in the base row!");
            throw new Error("No primary key field detected in the base row!");
        }

        const field: TField | null = this.getFieldById(baseField.id);

        if (field == null) {
            Logger.e("Field with id '" + baseField.id + "' not found!");
            throw new Error("Field with id '" + baseField.id + "' not found!");
        }

        return field;
    }

    /**
     *
     * @param baseRow
     */
    getPrimaryKeyFields(baseRow: TRow): TField[] {
        const baseFields: TField[] = baseRow.fields.filter(field => field instanceof TField && field.isPrimaryKey) as TField[];

        if (baseFields.length === 0) {
            Logger.e("No primary key field detected in the base row!");
            throw new Error("No primary key field detected in the base row!");
        }

        const fields: TField[] = baseFields.map(baseField => {
            const field = this.getFieldById(baseField.id);

            if (field == null) {
                Logger.e("Field with id '" + baseField.id + "' not found!");
                throw new Error("Field with id '" + baseField.id + "' not found!");
            }

            return field;
        });

        return fields;
    }

    getListById(id: string): TList | null {
        const list = this.lists.find((list) => list.id === id);
        return list ?? null;
    }

}

export default TRow;
