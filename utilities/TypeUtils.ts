const TypeUtils = {

    isString(value: any): value is string {
        return typeof value === "string" ;
    },

    isBoolean(value: any): value is boolean {
        return typeof value === "boolean" ;
    },

    getBooleanValue(value: string | boolean | null) {
        if (value == null) {
            return false;
        }

        if (typeof value === "string") {
            if (value.toLowerCase() === "y" || value.toLowerCase() === "true") {
                return true;
            }

            return false;
        }

        return value;
    }
}

export default TypeUtils;
