const NumberUtils = {
    /**
     * Round number to nbDecimal
     *
     * example:
     * roundDecimal(25.4512, 2)  => 25.45
     * roundDecimal(25.4565, 2)  => 25.46
     *
     * @param number The number to round
     * @param nbDecimal The number of decimal to keep
     */
    roundDecimal(number: number, nbDecimal: number) {
        // const n = Math.pow(10, nbDecimal);
        // return Math.round((number + Number.EPSILON) * n) / n;

        return +(Math.round(Number(number + `e+${nbDecimal}`)) + `e-${nbDecimal}`);
    }

};

export default NumberUtils;