const WebServiceUtils = {
    /**
     * Build an URI encoded query string with the given data.
     *
     * @param data The object containing the query data to encode
     * @return The query string in the form 'key1=value1&key2=value2&...'
     */
    buildURIQueryString(data: {[key:string]: string | number | boolean}): string {
        const ret = [];
        for (let d in data) {
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        }
        return ret.join('&');
    },
}

export default WebServiceUtils;
