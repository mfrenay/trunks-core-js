// eslint-disable-next-line import/no-anonymous-default-export
const ObjectUtils = {

    isNullOrEmptyObject(object) {
        if (!object || Object.keys(object).length === 0)
            return true;

        return false;
    },

    /**
     * For each property name of propertyMappings, map the property from source object to target object.
     *
     * Example:
     *  source = { "id": 3, "firstName": "John" }
     *  target = { "id": 4, "toto": "tata" }
     *  mappings = ["id", "firstName", "lastName"]
     *  Returns { "id": 3, "toto": "tata", "firstName": "John" }
     *
     * @param target A JSON object.
     *              Note: This object will be modified by the function.
     *              If target is an empty object, it will be updated with the given properties with the values
     *              taken from source if this property exists in source object.
     * @param source A JSON object.
     * @param propertyMappings A array of string representing the property names to map from the source to the target.
     * @returns {*} The updated target object.
     */
    mapObjectByProperties(target, source, propertyMappings) {
        for (let mapping of propertyMappings) {
            if (source[mapping])
                target[mapping] = source[mapping];
        }

        return target;
    },

    /**
     * Copy all properties of source into target, iff they are in the propertyMappings array.
     * If source is an array, consider the first item of this array.
     * @param target A JSON object. Note: if target is an empty object, it will be updated with the given properties with the values taken from source.
     * @param source A JSON object or an array of JSON object.
     * @param propertyMappings A array of string representing the property names to map from the source to the target.
     * @returns {{}} The target object with updated properties.
     */
    getMappedByPropertiesOrEmptyObject(target, source, propertyMappings) {
        if (source != null) {
            if (Array.isArray(source)) {
                if (source.length > 0) {
                    return this.mapObjectByProperties(target, source[0], propertyMappings);
                }
            }
            else {
                // Copy data
                return this.mapObjectByProperties(target, source, propertyMappings);
            }
        }

        return {};
    },

    /**
     * Copy all properties of source into target, iff these ones exist in target.
     * If source is an array, consider the first item of this array.
     * @param target A JSON object. Note: if target is an empty object, the return object will also be empty.
     * @param source A JSON object or an array of JSON object
     * @returns {{}} The target object with updated properties if the target was not empty.
     */
    getMappedOrEmptyObject(target, source) {
        if (source != null) {
            if (Array.isArray(source)) {
                if (source.length > 0) {
                    return this.mapObject(target, source[0]);
                }
            }
            else {
                // Copy data
                return this.mapObject(target, source);
            }
        }

        return {};
    },

    /**
     * Copy all properties of source into target, iff these ones exist in target.
     * @param target A JSON object
     * @param source A JSON object
     * @returns updated target object
     */
    mapObject(target, source) {
        for (const key of Object.keys(source)) {
            if (key in target) {
                target[key] = source[key];
            }
        }

        return target;
    },

    /**
     * TODO
     *
     * From StackOverflow: https://stackoverflow.com/a/32922084/4757919
     *
     * @param x
     * @param y
     * @param omitKeys
     * @return {boolean}
     */
    deepEquals(x, y, omitKeys = []) {
        const ok = Object.keys, tx = typeof x, ty = typeof y;

        if (x && y && tx === 'object' && tx === ty && x instanceof Date && y instanceof Date) {
            return x.getTime() === y.getTime();
        }

        return x && y && tx === 'object' && tx === ty ? (
            ok(x).every(key => omitKeys.includes(key) ? true : this.deepEquals(x[key], y[key], omitKeys))
        ) : (x === y);
    },


    /*deepEquals() {
        let i, l, leftChain, rightChain;

        function compare2Objects(x, y) {
            let p;

            // remember that NaN === NaN returns false
            // and isNaN(undefined) returns true
            if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
                return true;
            }

            // Compare primitives and functions.
            // Check if both arguments link to the same object.
            // Especially useful on the step where we compare prototypes
            if (x === y) {
                return true;
            }

            // Works in case when functions are created in constructor.
            // Comparing dates is a common scenario. Another built-ins?
            // We can even handle functions passed across iframes
            if ((typeof x === 'function' && typeof y === 'function') ||
                (x instanceof Date && y instanceof Date) ||
                (x instanceof RegExp && y instanceof RegExp) ||
                (x instanceof String && y instanceof String) ||
                (x instanceof Number && y instanceof Number)) {
                return x.toString() === y.toString();
            }

            // At last checking prototypes as good as we can
            if (!(x instanceof Object && y instanceof Object)) {
                return false;
            }

            if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
                return false;
            }

            if (x.constructor !== y.constructor) {
                return false;
            }

            if (x.prototype !== y.prototype) {
                return false;
            }

            // Check for infinitive linking loops
            if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
                return false;
            }

            // Quick checking of one object being a subset of another.
            // todo: cache the structure of arguments[0] for performance
            for (p in y) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                } else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }
            }

            for (p in x) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                } else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }

                switch (typeof (x[p])) {
                    case 'object':
                    case 'function':

                        leftChain.push(x);
                        rightChain.push(y);

                        if (!compare2Objects(x[p], y[p])) {
                            return false;
                        }

                        leftChain.pop();
                        rightChain.pop();
                        break;

                    default:
                        if (x[p] !== y[p]) {
                            return false;
                        }
                        break;
                }
            }

            return true;
        }

        if (arguments.length < 1) {
            return true; //Die silently? Don't know how to handle such case, please help...
            // throw "Need two or more arguments to compare";
        }

        for (i = 1, l = arguments.length; i < l; i++) {

            leftChain = []; //Todo: this can be cached
            rightChain = [];

            if (!compare2Objects(arguments[0], arguments[i])) {
                return false;
            }
        }

        return true;
    },*/

    /**
     * TODO
     *
     * From StackOverflow: https://stackoverflow.com/a/34705171/4757919
     *
     * @param obj
     * @param omitKeys
     */
    omit(obj, omitKeys = []) {
        return Object.keys(obj).reduce((result, key) => {
            if (!omitKeys.includes(key)) {
                result[key] = obj[key];
            }
            return result;
        }, {});
    },

    /**
     *
     * @param obj
     * @param predicate
     * @return {T}
     */
    filterObject(obj, predicate) {
        return Object.keys(obj)
            .filter( key => predicate(key) )
            .reduce( (res, key) => {res[key] = obj[key]; return res;}, {} );
    }

}

export default ObjectUtils;
