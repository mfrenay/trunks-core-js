import StringUtils from "./StringUtils";
import TField from "../model/TField";

type TFunction = (key: string | string[], options?: any) => string;

/**
 * translate field value according context and if the field must be translated
 * @param value
 * @param baseField
 * @param t
 */
export function translate(value: string, baseField: TField, t: TFunction | null = null) {
  const translation_context = baseField.customObjectParams?.TRANSLATION_CONTEXT;
  if (StringUtils.getNString(translation_context) !== "" && t != null) {
    return t(translation_context + ":" + value);
  }

  const translate = StringUtils.stringToBoolean(baseField.customObjectParams?.TRANSLATE);
  if (translate && t != null) {
    return t(value);
  }

  return value;
}

/**
 * Get translation and return default value if no translation was found
 * @param t
 * @param translationKey
 * @param defaultValue
 */
export function ntranslate(t: TFunction, translationKey: string, defaultValue: string | null = null) {
  const translation = t(translationKey);
  let key = translationKey;

  if (key.indexOf(":")) {
    const parts = key.split(":");
    key = parts[parts.length-1];
  }

  if (translation === key && defaultValue !== null) {
    return defaultValue;
  }

  return translation;
}
