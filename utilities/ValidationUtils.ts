/**
 * Validate that the NISS and the birthdate are consistent
 * The NISS must begin with the following format YYMMDD derived from the birthdate
 *
 * @param niss The national number
 * @param birthDate The birthdate in string format
 */
export function checkNissMatchBirthdate(niss: string, birthDate: string): boolean {
    const date = new Date(birthDate);

    const zeroPad = (num: number, places: number) => String(num).padStart(places, '0');

    const str = date.getFullYear().toString().substring(2, 4)
        + zeroPad(date.getMonth() + 1, 2)
        + zeroPad(date.getDate(), 2).toString();

    return niss.substring(0, 6) === str;
}

/**
 * Check the validity of the given email address
 * @param email
 */
export function checkEmail(email: string): boolean {
    if (email.length > 150) {
        return false;
    }

    const regex = new RegExp("^([a-zA-Z0-9])+([\\.]{0,1}[a-zA-Z0-9\\-_]+)*@(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,})$");
    return regex.test(email);
}

/**
 *
 * @param phone
 */
export function checkPhone(phone: string): boolean {
    const regex = new RegExp("^[+][0-9]{1,4}[0-9]{8,16}$");

    return  regex.test(phone);
}

/**
 * Check whether the email has a temporary domain.
 *
 * @param email String
 * @return boolean, true if the email has a temporary email domain, false otherwise
 */
export function hasTemporaryEmailDomain(email: string): boolean {
    return (email.indexOf("@yopmail.") !== -1 ||
      email.indexOf("@ailinator.") !== -1 ||
      email.indexOf("@jetable.") !== -1 ||
      email.indexOf("@trashmail.") !== -1 ||
      email.indexOf("@tempmail.") !== -1 ||
      email.indexOf("@guerrillamail.") !== -1 ||
      email.indexOf("@10minutemail.") !== -1 ||
      email.indexOf("@throwawaymail.") !== -1 ||
      email.indexOf("@emailondeck.") !== -1 ||
      email.indexOf("@emailfake.") !== -1 ||
      email.indexOf("@e4ward.") !== -1 ||
      email.indexOf("@dispostable.") !== -1 ||
      email.indexOf("@smailpro.") !== -1 ||
      email.indexOf("@getnada.") !== -1);
}
