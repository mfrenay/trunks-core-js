import StringUtils from "../StringUtils";

describe('stringUtils.replaceAll', () => {
    it('Should remove dash and two points', () => {
        const result = StringUtils.replaceAll(StringUtils.replaceAll("2020-06-16T09:00:00", "-", ""), ":", "");
        expect(result).toEqual("20200616T090000");
    });
});

describe('stringUtils.replaceParametersInExpression', () => {
    it('Should replace parameters', () => {
        const result = StringUtils.replaceParametersInExpression("Calyora founders are #1# and #2#", ["Seb", "Mich"]);
        expect(result).toEqual("Calyora founders are Seb and Mich");
    });
});

describe('stringUtils.containsDigit', () => {
    it('Should return true if string contains at least one digit', () => {
        const result = StringUtils.containsDigit("test-2-test");
        expect(result).toEqual(true);
    });
    it('Should return false if string does not contain any digit', () => {
        const result = StringUtils.containsDigit("test@#'}");
        expect(result).toEqual(false);
    });
});

