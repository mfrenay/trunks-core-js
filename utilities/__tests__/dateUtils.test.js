import DateUtils from "../DateUtils";

describe('dateUtils.lastWeekDayOfMonth', () => {
    it('get last friday Date object', () => {
        const date = DateUtils.lastWeekDayOfMonth(5, 3, 2022); // 5 = Friday, 3 = March
        expect(date).toEqual(new Date('2022-03-25'));

    });
});

describe('dateUtils.getCurrentHourHHMMSS', () => {
    it('converts Date object', () => {
        const dateHHMMSS = DateUtils.getCurrentHourHHMMSS();
        const format = 'HH:MM:SS';
        expect(dateHHMMSS.length).toEqual(format.length);

        const regex = new RegExp('^\\d{2}:\\d{2}:\\d{2}$');
        expect(regex.test(dateHHMMSS)).toBe(true);
    });
});

describe('dateUtils.getFormattedDateYYYYMMDD', () => {
    it('should return date formatted string on Date object argument', () => {
        const date = new Date(2020, 5, 25, 10, 0);
        const dateYYYMMDD = DateUtils.getFormattedDateYYYYMMDD(date);
        expect(dateYYYMMDD).toEqual('2020-06-25');
    });

    it('should return null on null date argument', () => {
        const dateYYYMMDD = DateUtils.getFormattedDateYYYYMMDD(null);
        expect(dateYYYMMDD).toEqual(null);
    });

    it('should return null on undefined date argument', () => {
        const dateYYYMMDD = DateUtils.getFormattedDateYYYYMMDD(undefined);
        expect(dateYYYMMDD).toEqual(null);
    });
});

describe('dateUtils.getFormattedDateDDMMYYYY', () => {
    it('converts Date object', () => {
        const date = new Date(2020, 5, 25, 10, 0);
        const dateDDMMYYYY = DateUtils.getFormattedDateDDMMYYYY(date);
        expect(dateDDMMYYYY).toEqual('25/06/2020');
    });

    it('should return a DD/MM/YYYY formatted date string on string date format argument', () => {
        const dateDDMMYYYY = DateUtils.getFormattedDateDDMMYYYY("1979-10-10");
        expect(dateDDMMYYYY).toEqual('10/10/1979');
    });

    it('Throws an exception when the date is an empty string and there is no defaultReturn', () => {
        try {
            DateUtils.getFormattedDateDDMMYYYY("");
            expect(true).toBe(false);
        } catch (e) {
            // Test must catch an exception
        }
    });

    it('Return defaultReturn when the date is an empty string and there is defaultReturn', () => {
        try {
            const dateDDMMYYYY = DateUtils.getFormattedDateDDMMYYYY("", "");
            expect(dateDDMMYYYY).toEqual("");
        } catch (e) {
            expect(true).toBe(false);
        }
    });

    it('Return null when the date is in a wrong string format and defaultReturn = null', () => {
        try {
            const dateDDMMYYYY = DateUtils.getFormattedDateDDMMYYYY("wrong date", null);
            expect(dateDDMMYYYY).toEqual(null);
        } catch (e) {
            expect(true).toBe(false);
        }
    });

    it('Throws an exception when the date is in a wrong string format and there is no defaultReturn', () => {
        try {
            DateUtils.getFormattedDateDDMMYYYY("wrong date");
            expect(true).toBe(false);
        } catch (e) {
            // Test must catch an exception
        }
    });

    it('', () => {

        // const dateDDMMYYYY = dateUtils.getFormattedDateDDMMYYYY("", "empty date");
        // const dateDDMMYYYY = dateUtils.getFormattedDateDDMMYYYY("wrong date", "");
    });
});

describe('dateUtils.lastMonday', () => {
    it('returns the last monday before a specific date (where date is a Monday)', () => {
        const result = DateUtils.previousMonday(new Date("2020-08-03"));
        const expectedDate = new Date("2020-08-03");

        expect(result).toEqual(expectedDate);
    });

    it('returns the last monday before a specific date (where date is a Tuesday)', () => {
        const result = DateUtils.previousMonday(new Date("2020-08-04"));
        const expectedDate = new Date("2020-08-03");

        expect(result).toEqual(expectedDate);
    });

    it('returns the last monday before a specific date (where date is a Wednesday)', () => {
        const result = DateUtils.previousMonday(new Date("2020-08-05"));
        const expectedDate = new Date("2020-08-03");

        expect(result).toEqual(expectedDate);
    });

    it('returns the last monday before a specific date (where date is a Thursday)', () => {
        const result = DateUtils.previousMonday(new Date("2020-08-06"));
        const expectedDate = new Date("2020-08-03");

        expect(result).toEqual(expectedDate);
    });

    it('returns the last monday before a specific date (where date is a Friday)', () => {
        const result = DateUtils.previousMonday(new Date("2020-08-07"));
        const expectedDate = new Date("2020-08-03");

        expect(result).toEqual(expectedDate);
    });

    it('returns the last monday before a specific date (where date is a Saturday)', () => {
        const result = DateUtils.previousMonday(new Date("2020-08-08"));
        const expectedDate = new Date("2020-08-03");

        expect(result).toEqual(expectedDate);
    });

    it('returns the last monday before a specific date (where date is a Sunday)', () => {
        const result = DateUtils.previousMonday(new Date("2020-08-09"));
        const expectedDate = new Date("2020-08-03");

        expect(result).toEqual(expectedDate);
    });
});

describe('dateUtils.nextMonday', () => {
    it('returns the next monday of a specific date (where date is a Monday)', () => {
        const result = DateUtils.nextMonday(new Date("2020-08-03"));
        const expectedDate = new Date("2020-08-10");

        expect(result).toEqual(expectedDate);
    });

    it('returns the next monday of a specific date (where date is a Tuesday)', () => {
        const result = DateUtils.nextMonday(new Date("2020-08-04"));
        const expectedDate = new Date("2020-08-10");

        expect(result).toEqual(expectedDate);
    });

    it('returns the next monday of a specific date (where date is a Wednesday)', () => {
        const result = DateUtils.nextMonday(new Date("2020-08-05"));
        const expectedDate = new Date("2020-08-10");

        expect(result).toEqual(expectedDate);
    });

    it('returns the next monday of a specific date (where date is a Thursday)', () => {
        const result = DateUtils.nextMonday(new Date("2020-08-06"));
        const expectedDate = new Date("2020-08-10");

        expect(result).toEqual(expectedDate);
    });

    it('returns the next monday of a specific date (where date is a Friday)', () => {
        const result = DateUtils.nextMonday(new Date("2020-08-07"));
        const expectedDate = new Date("2020-08-10");

        expect(result).toEqual(expectedDate);
    });

    it('returns the next monday of a specific date (where date is a Saturday)', () => {
        const result = DateUtils.nextMonday(new Date("2020-08-08"));
        const expectedDate = new Date("2020-08-10");

        expect(result).toEqual(expectedDate);
    });

    it('returns the next monday of a specific date (where date is a Sunday)', () => {
        const result = DateUtils.nextMonday(new Date("2020-08-09"));
        const expectedDate = new Date("2020-08-10");

        expect(result).toEqual(expectedDate);
    });
});

describe('isValidDate()', () => {
    it('valid date', () => {
        const result = DateUtils.isValidDate(new Date("2020-08-21"));

        expect(result).toEqual(true);
    });

    it('valid string date', () => {
        const result = DateUtils.isValidDate("2020-08-21");

        expect(result).toEqual(true);
    });

    it('invalid string date', () => {
        const result = DateUtils.isValidDate("not valid");

        expect(result).toEqual(false);
    });

    it('invalid null date', () => {
        const result = DateUtils.isValidDate(null);

        expect(result).toEqual(false);
    });

    it('invalid empty string', () => {
        const result = DateUtils.isValidDate("");

        expect(result).toEqual(false);
    });

    it.skip('invalid string (finishing by a number)', () => {
        const result = DateUtils.isValidDate("bla 2");

        expect(result).toEqual(false);
    });
});

describe('sameDay()', () => {
    it('should return true for two dates that occur the same day', () => {
        const result = DateUtils.sameDay(new Date("2020-08-21T10:00:00Z"), new Date("2020-08-21T16:00:00Z"));

        expect(result).toEqual(true);
    });

    it('should return false for two dates that does not occur the same day', () => {
        const result = DateUtils.sameDay(new Date("2020-08-21T10:00:00Z"), new Date("2020-08-22T16:00:00Z"));

        expect(result).toEqual(false);
    });
});


describe('formattedTimeRange()', () => {
    it('should return a correct formatted time range', () => {
        const result = DateUtils.formattedTimeRange(new Date("2020-08-21T10:00:00Z"), new Date("2020-08-21T16:00:00Z"), true);

        expect(result).toEqual("10:00 - 16:00");
    });
});

describe('hoursStringToTime()', () => {
    it('should return the good time', () => {
        const result = DateUtils.hoursStringToTime("00:45");

        expect(result).toEqual(45);
    });

    it('should return the good time 2', () => {
        const result = DateUtils.hoursStringToTime("03:19");

        expect(result).toEqual(199);
    });

    it('should return the good time 3', () => {
        const result = DateUtils.hoursStringToTime("00:1s");

        expect(result).toEqual(null);
    });
});

describe('timeToHoursString()', () => {
    it('should return the good time string for time less than an hour', () => {
        const result = DateUtils.timeToHoursString(45);

        expect(result).toEqual("00:45");
    });

    it('should return the good time string for time more than an hour', () => {
        const result = DateUtils.timeToHoursString(264);

        expect(result).toEqual("04:24");
    });

    it('should return the good time string for time is a string representing a number', () => {
        const result = DateUtils.timeToHoursString("75");

        expect(result).toEqual("01:15");
    });

    it('should return null if the time is not correct', () => {
        const result = DateUtils.timeToHoursString("abc");

        expect(result).toEqual(null);
    });
});

describe('hoursStringAddInterval()', () => {
    it('should return midnight (when interval hours string - interval is below midnight)', () => {
        const result = DateUtils.hoursStringAddInterval("01:00", -120);

        expect(result).toEqual("00:00");
    });

    it('should return the hours string with the interval added (more than an hour)', () => {
        const result = DateUtils.hoursStringAddInterval("08:00", -120);

        expect(result).toEqual("06:00");
    });

    it('should return the hours string with the interval added (less than an hour)', () => {
        const result = DateUtils.hoursStringAddInterval("20:00", 20);

        expect(result).toEqual("20:20");
    });

    it('should return 24:00 (when interval hours string + interval is after midnight)', () => {
        const result = DateUtils.hoursStringAddInterval("22:00", 120);

        expect(result).toEqual("24:00");
    });
});


describe('setMinutes', () => {
    it('should return a new Date object based on the given date and the given minutes', () => {
        const date = new Date("2021-03-12T10:00:00.000Z");
        const result = DateUtils.setMinutes(date, 20);

        expect(date === result).toEqual(false);
        expect(date.getMinutes()).toEqual(0);
        expect(result.getMinutes()).toEqual(20);
    });
});

describe('setHours', () => {
    it('should return a new Date object based on the given date and the given hours', () => {
        const date = new Date("2021-03-12T10:00:00.000");
        const result = DateUtils.setHours(date, 5);

        expect(date === result).toEqual(false);
        expect(date.getHours()).toEqual(10);
        expect(result.getHours()).toEqual(5);
    });
});

describe('toLocalIsoString', () => {
    it('should return an ISO local date string (before DST)', () => {
        const date = new Date("2021-03-25T10:00:00.000Z");
        const result = DateUtils.toLocalIsoString(date);

        expect(date.toISOString()).toEqual("2021-03-25T10:00:00.000Z");
        expect(result.includes("Z")).toEqual(false);
        expect(new Date(result)).toEqual(date);
    });

    it('should return an ISO local date string (after DST)', () => {
        const date = new Date("2021-04-02T15:00:00.000Z");
        const result = DateUtils.toLocalIsoString(date);

        expect(date.toISOString()).toEqual("2021-04-02T15:00:00.000Z");
        expect(result.includes("Z")).toEqual(false);
        expect(new Date(result)).toEqual(date);
    });
});

// !!!
// !!! We consider the test is running on a device that is in CET timezone (ie. UTC+1 in winter and UTC+2 in summer)
// !!!
describe('convertTZ', () => {
    test('Asia/Tokyo in witer time', () => {
        const dateTZ = DateUtils.convertTZ(new Date("2022-01-20T10:00:00.000+09:00"), "Asia/Tokyo"); // UTC+9

        expect(dateTZ).toEqual(new Date("2022-01-20T10:00:00.000+01:00"));
    });

    test('Asia/Tokyo in summer time', () => {
        const dateTZ = DateUtils.convertTZ(new Date("2022-08-01T10:00:00.000+09:00"), "Asia/Tokyo"); // UTC+9

        expect(dateTZ).toEqual(new Date("2022-08-01T10:00:00.000+02:00"));
    });

    test('America/New_York in winter time', () => {
        const dateTZ = DateUtils.convertTZ(new Date("2022-03-01T10:00:00.000-05:00"), "EST"); // UTC-5

        expect(dateTZ).toEqual(new Date("2022-03-01T10:00:00.000+01:00"));
    });

    test('America/New_York in summer time', () => {
        const dateTZ = DateUtils.convertTZ(new Date("2022-08-01T10:00:00.000-05:00"), "EST"); // UTC-5

        expect(dateTZ).toEqual(new Date("2022-08-01T10:00:00.000+02:00"));
    });

    test('CET in winter time', () => {
        const dateTZ = DateUtils.convertTZ(new Date("2022-03-01T10:00:00.000+01:00"), "CET"); // UTC+1

        expect(dateTZ).toEqual(new Date("2022-03-01T10:00:00.000+01:00")); // -01 + 01 (local winter offset) = 00
    });

    test('CET in summer time', () => {
        const dateTZ = DateUtils.convertTZ(new Date("2022-05-01T10:00:00.000+02:00"), "CET"); // UTC+2

        expect(dateTZ).toEqual(new Date("2022-05-01T10:00:00.000+02:00"));
    });
});
