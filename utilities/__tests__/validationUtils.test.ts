import {checkEmail, checkNissMatchBirthdate, checkPhone, hasTemporaryEmailDomain} from "../ValidationUtils";

describe('checkNissMatchBirthdate', () => {
    test('Valid NISS', () => {
        const result = checkNissMatchBirthdate("93052232133", "1993-05-22T00:00:00Z");

        expect(result).toEqual(true);
    });

    test('Inalid NISS', () => {
        const result = checkNissMatchBirthdate("93062232133", "1993-05-22T00:00:00Z");

        expect(result).toEqual(false);
    });
});

describe('checkEmail', () => {
    test('Valid Email', () => {
        expect(checkEmail("simple@example.com")).toEqual(true);
        expect(checkEmail("very.common@example.com")).toEqual(true);
        expect(checkEmail("disposable.style.email.with.symbol@example.com")).toEqual(true);
        expect(checkEmail("other.email-with-hyphen@example.com")).toEqual(true);
        expect(checkEmail("fully-qualified_domain@example.com")).toEqual(true);
        expect(checkEmail("fully-qualified_----___.---_-_-domain@example.com")).toEqual(true);
        expect(checkEmail("x@example.com")).toEqual(true);
        expect(checkEmail("example-indeed@strange-example.com")).toEqual(true);
        expect(checkEmail("example@s.example")).toEqual(true);
        expect(checkEmail("simplenumber3@example.com")).toEqual(true);
        expect(checkEmail("simplenumber3@examplenumber10.com")).toEqual(true);
        expect(checkEmail("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890@123456789012345.com")).toEqual(true);
    });

    test('Invalid Email', () => {
        expect(checkEmail("a@a")).toEqual(false);
        expect(checkEmail("a")).toEqual(false);
        expect(checkEmail("user.name+tag+sorting@example.com")).toEqual(false);
        expect(checkEmail("admin@mailserver1")).toEqual(false); // local domain name with no TLD, although ICANN highly discourages dotless email addresses
        expect(checkEmail("' '@example.org")).toEqual(false); // space between the quotes
        expect(checkEmail("'john..doe'@example.org")).toEqual(false); // quoted double dot
        expect(checkEmail("mailhost!username@example.org")).toEqual(false); // bangified host route used for uucp mailers
        expect(checkEmail("user%example.com@example.org")).toEqual(false);
        expect(checkEmail("Abc.example.com")).toEqual(false); // (no @ character)
        expect(checkEmail("AA@b@c@example.com ")).toEqual(false); // (only one @ is allowed outside quotation marks)
        expect(checkEmail("a\"b(c)d,e:f;g<h>i[j\\k]l@example.com ")).toEqual(false); // (none of the special characters in this local-part are allowed outside quotation marks)
        expect(checkEmail("just\"not\"right@example.com")).toEqual(false); // (quoted strings must be dot separated or the only element making up the local-part)
        expect(checkEmail("this is\"not\\allowed@example.com")).toEqual(false); // (spaces, quotes, and backslashes may only exist when within quoted strings and preceded by a backslash)
        expect(checkEmail("this\\ still\\\"not\\\\allowed@example.com")).toEqual(false); // (even if escaped (preceded by a backslash), spaces, quotes, and backslashes must still be contained by quotes)
        expect(checkEmail("12345.6.7.89012.34567.89012..34567.89012.34569898590.xxxx@example.com")).toEqual(false); // (local part is longer than 64 characters)
        expect(checkEmail("i_like_underscore@but_its_not_allow_in _this_part.example.com")).toEqual(false);// (domain can not contain backslash)
    });
});

describe('checkPhone', () => {
    test('Valid phone', () => {
        expect(checkPhone("+3243559812")).toEqual(true); // Belgian fix number
        expect(checkPhone("+32475123456")).toEqual(true); // Belgian cell number
    });
    test('Invalid phone', () => {
        expect(checkPhone("043559812")).toEqual(false); // without country code
        expect(checkPhone("0e43559812")).toEqual(false); // with alphabetic character
        expect(checkPhone("+32123")).toEqual(false); // number too short
        expect(checkPhone("+3212345678901234567890")).toEqual(false); // number too long
        expect(checkPhone("+3212345678+")).toEqual(false); // invalid character at the end
        expect(checkPhone("+32 4 355 98 12")).toEqual(false); // spaces
        expect(checkPhone("+32475/123456")).toEqual(false); // slashes
        expect(checkPhone("+32475.12.34.56")).toEqual(false); // dots
    });
});

describe('hasTemporaryEmailDomain', () => {
    test('Valid domain', () => {
        expect(hasTemporaryEmailDomain("example@gmail.com")).toEqual(false);
        expect(hasTemporaryEmailDomain("example@hotmail.com")).toEqual(false);
        expect(hasTemporaryEmailDomain("example@outlook.com")).toEqual(false);
        expect(hasTemporaryEmailDomain("example@icloud.com")).toEqual(false);
    });
    test('Invalid domain', () => {
        expect(hasTemporaryEmailDomain("@ailinator.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@jetable.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@trashmail.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@tempmail.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@guerrillamail.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@10minutemail.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@throwawaymail.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@emailondeck.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@emailfake.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@e4ward.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@dispostable.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@smailpro.")).toEqual(true);
        expect(hasTemporaryEmailDomain("@getnada.")).toEqual(true);
    });
});
