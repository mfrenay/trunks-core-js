import objectUtils from "../ObjectUtils";

describe('objectUtils.isNullOrEmptyObject', () => {
    it('Returns true for undefined object argument', () => {
        const ret = objectUtils.isNullOrEmptyObject(undefined);
        expect(ret).toEqual(true);
        //expect(objectUtils.isNullOrEmptyObject(undefined)).to.be.true;
    });

    it('Returns true for null object argument', () => {
        const ret = objectUtils.isNullOrEmptyObject(null);
        expect(ret).toEqual(true);
        //expect(objectUtils.isNullOrEmptyObject(undefined)).to.be.true;
    });

    it('Returns true for empty object argument', () => {
        const object = {}
        const ret = objectUtils.isNullOrEmptyObject(object);
        expect(ret).toEqual(true);
        //expect(objectUtils.isNullOrEmptyObject(undefined)).to.be.true;
    });

    it('Returns false for a non empty object argument', () => {
        const object = { "foo": "bar" }
        const ret = objectUtils.isNullOrEmptyObject(object);
        expect(ret).toEqual(false);
        //expect(objectUtils.isNullOrEmptyObject(undefined)).to.be.true;
    });
});

describe('objectUtils.mapObjectByProperties', () => {
    it('', () => {
        // TODO
    });
});


describe('objectUtils.getMappedByPropertiesOrEmptyObject', () => {
    it('', () => {
        // TODO
    });
});

describe('objectUtils.getMappedOrEmptyObject', () => {
    it('', () => {
        // TODO
    });
});

describe('objectUtils.mapObject', () => {
    it('', () => {
        // TODO
    });
});

describe('objectUtils.deepEquals', () => {
    it('compare with same references', () => {
        const obj1 = {a: "a", b: "b", c: "c"};
        const obj2 = obj1;

        const result = objectUtils.deepEquals(obj1, obj2);

        expect(result).toStrictEqual(true);
    });

    it('compare with same object (different references)', () => {
        const obj1 = {a: "a", b: "b", c: "c"};
        const obj2 = {a: "a", b: "b", c: "c"};

        const result = objectUtils.deepEquals(obj1, obj2);

        expect(result).toStrictEqual(true);
    });

    it('compare with different object', () => {
        const obj1 = {a: "a", b: "b", c: "c"};
        const obj2 = {a: "a", b: "b"};

        const result = objectUtils.deepEquals(obj1, obj2);

        expect(result).toStrictEqual(false);
    });

    it('compare with same object (multiple levels)', () => {
        const obj1 = {a: "a", b: "b", c: {d: {e: "e"}}, g: "g"};
        const obj2 = {a: "a", b: "b", c: {d: {e: "e"}}, g: "g"};

        const result = objectUtils.deepEquals(obj1, obj2);

        expect(result).toStrictEqual(true);
    });

    it('compare with different object (multiple levels)', () => {
        const obj1 = {a: "a", b: "b", c: {d: {e: "e"}}, g: "g"};
        const obj2 = {a: "a", b: "b", c: {d: {e: "f"}}, g: "g"};

        const result = objectUtils.deepEquals(obj1, obj2);

        expect(result).toStrictEqual(false);
    });

    it('compare with different object (multiple levels) with omit', () => {
        const obj1 = {a: "a", b: "b", c: {d: {e: "e"}}, g: "g"};
        const obj2 = {a: "a", b: "b", c: {d: {e: "f"}}};

        const result = objectUtils.deepEquals(obj1, obj2, ["e", "g"]);

        expect(result).toStrictEqual(true);
    });

    it('compare with same dates', () => {
        const obj1 = {a: "a", b: new Date("2020-01-01T10:00:00.000Z")};
        const obj2 = {a: "a", b: new Date("2020-01-01T10:00:00.000Z")};

        const result = objectUtils.deepEquals(obj1, obj2);

        expect(result).toStrictEqual(true);
    });

    it('compare with different dates', () => {
        const obj1 = {a: "a", b: new Date("2020-01-01T10:00:00.000Z")};
        const obj2 = {a: "a", b: new Date("2020-01-01T10:30:00.000Z")};

        const result = objectUtils.deepEquals(obj1, obj2);

        expect(result).toStrictEqual(false);
    });
});

describe('objectUtils.omit', () => {
    it('no keys argument', () => {
        const result = objectUtils.omit({a: "a", b: "b", c: "c"});

        expect(result).toStrictEqual({a: "a", b: "b", c: "c"});
    });

    it('empty array keys argument', () => {
        const result = objectUtils.omit({a: "a", b: "b", c: "c"}, []);

        expect(result).toStrictEqual({a: "a", b: "b", c: "c"});
    });

    it('omits one key', () => {
        const result = objectUtils.omit({a: "a", b: "b", c: "c"}, ["b"]);

        expect(result).toStrictEqual({a: "a", c: "c"});
    });

    it('omits multiple keys', () => {
        const result = objectUtils.omit({a: "a", b: "b", c: "c"}, ["b", "c"]);

        expect(result).toStrictEqual({a: "a"});
    });
});

