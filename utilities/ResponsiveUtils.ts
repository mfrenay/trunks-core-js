const ResponsiveUtils = {
    /**
     * Device option
     * Return the given value according to the current device (based on window size)
     *
     * @param mobile Value to return if the current device is a mobile
     * @param desktop Value to return if the current device is a desktop
     */
    deviceOpt<T>({mobile, desktop}: {mobile?: T, desktop: T}): T {
        if (window.screen.width < 1024 && mobile != null) {
            return mobile;
        } else {
            return desktop;
        }
    },
}

export default ResponsiveUtils;
