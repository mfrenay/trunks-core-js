import StringUtils from "./StringUtils";
import Logger from "../logger/logger";

export const DAYS_OF_WEEK = {
    "0" : "Dimanche",
    "1" : "Lundi",
    "2" : "Mardi",
    "3" : "Mercredi",
    "4" : "Jeudi",
    "5" : "Vendredi",
    "6" : "Samedi",
}

export const NUMERAL_ADJECTIVE = {
    1 : "premier",
    2 : "deuxième",
    3 : "troisième",
    4 : "quatrième",
    5 : "cinquième"
}

const DateUtils = {
    /**
     * Get the current hour with format HH:MM:SS
     * @returns {string}
     */
    getCurrentHourHHMMSS() {
        let day = new Date();
        let hour = Math.max(day.getHours(), 1) - 1;
        let strHour = hour + ":00:00";

        return strHour.length === 7 ? "0" + strHour : strHour;
    },

    /**
     *
     * Note: the month index of the Date type begins at 0
     * @param date {string|Date}
     * @returns {string|null}
     */
    getFormattedDateDDMM(date) {
        if (date == null) return null;

        const d = new Date(date);

        try {
            return StringUtils.lpad(d.getDate(), 2, "0") + "/" + StringUtils.lpad(d.getMonth() + 1, 2, "0");
        } catch (e) {
            Logger.e(`dateUtils.getFormattedDateDDMM(${date}) - Error: ${e.toLocaleString()}`)
            return null;
        }
    },

    /**
     *
     * Note: the month index of the Date type begins at 0
     * @param date {string|Date}
     * @returns {string|null}
     */
    getFormattedDateYYYYMMDD(date) {
        if (date == null) return null;

        const d = new Date(date);

        try {
            return d.getFullYear() + "-" + StringUtils.lpad(d.getMonth() + 1, 2, "0") + "-" + StringUtils.lpad(d.getDate(), 2, "0")
        } catch (e) {
            Logger.e(`dateUtils.getFormattedDateYYYYMMDD(${date}) - Error: ${e.toLocaleString()}`)
            return null;
        }
    },

    /**
     *
     * @param date {string|Date}
     * @param defaultReturn {string|undefined}
     * @returns {string} or Exception
     */
    getFormattedDateDDMMYYYY(date, defaultReturn = undefined) {
        try {
            if (date == null || date === "") {
                throw new Error("'date' param must be a non-empty string or a Date object");
            }

            const d = new Date(date);

            if (!this.isValidDate(d)) {
                Logger.w("'date' (" + date + ") param must be a date-formatted string or a Date object", d);
                throw new Error("'date' param must be a date-formatted string or a Date object");
            }

            return StringUtils.lpad(d.getDate(), 2, "0") + "/" + StringUtils.lpad(d.getMonth() + 1, 2, "0") + "/" + d.getFullYear();
        } catch (e) {
            if (defaultReturn === undefined) {
                Logger.e(`dateUtils.getFormattedDateDDMMYYYY(${date}) - Error: ${e.toLocaleString()}`);
                throw e;
            } else {
                return defaultReturn;
            }
        }
    },

    /**
     *
     * @param date {string|Date}
     * @param inUTC True if the returned formatted date must be in UTC, false if it must be in the local timezone. (default is false)
     * @returns {string}
     */
    getFormattedHourHHMM(date, inUTC = false) {
        const d = date instanceof Date ? date : new Date(date);

        return StringUtils.lpad((inUTC ? d.getUTCHours() : d.getHours()) + "", 2, "0") + ":" + StringUtils.lpad((inUTC ? d.getUTCMinutes() : d.getMinutes()) + "", 2, "0");
    },

    /**
     *
     * @param date {string|Date}
     * @param inUTC
     * @returns {string}
     */
    getFormattedHourHHMMSS(date, inUTC = false) {
        return this.getFormattedHourHHMM(date, inUTC) + ":00";
    },

    /**
     *
     * @param date
     * @returns {string}
     */
    getFormattedDateDDMMYYYYHHMM(date) {
        return this.getFormattedDateDDMMYYYY(date) + " " + this.getFormattedHourHHMM(date);
    },

    /**
     *
     * @param milliseconds
     * @returns {string}
     */
    msToHHMMSS(milliseconds) {
        // 1- Convert into seconds:
        let seconds = milliseconds / 1000;

        // 2- Extract hours:
        let hours = parseInt(seconds / 3600); // 3,600 seconds in 1 hour
        seconds = seconds % 3600; // seconds remaining after extracting hours

        // 3- Extract minutes:
        const minutes = parseInt(seconds / 60); // 60 seconds in 1 minute

        // 4- Keep only seconds not extracted to minutes:
        seconds = parseInt(seconds % 60);

        return (hours > 9 ? hours : "0" + hours) + ":" + (minutes > 9 ? minutes : "0" + minutes) + ":" + (seconds > 9 ? seconds : "0" + seconds);
    },

    /**
     *
     * @param milliseconds
     * @returns {string}
     */
    msToHHMM(milliseconds) {
        // 1- Convert into seconds:
        let seconds = milliseconds / 1000;

        // 2- Extract hours:
        let hours = parseInt(seconds / 3600); // 3,600 seconds in 1 hour
        seconds = seconds % 3600; // seconds remaining after extracting hours

        // 3- Extract minutes:
        const minutes = parseInt(seconds / 60); // 60 seconds in 1 minute

        return (hours > 9 ? hours : "0" + hours) + ":" + (minutes > 9 ? minutes : "0" + minutes);
    },

    /**
     *
     * @param date
     * @returns {number}
     */
    weekNumberInMonth(date) {
        let weekNumber = 1
        let newDate = new Date(date)
        newDate.setDate(newDate.getDate() - 7)

        while (newDate.getMonth() === date.getMonth()) {
            weekNumber++
            newDate.setDate(newDate.getDate() - 7)
        }

        return weekNumber;
    },

    /**
     *
     * @param date {string|Date}
     * @returns {string}
     */
    formattedWeekNumberInMonth(date) {
        const weekNumber = this.weekNumberInMonth(date);
        return NUMERAL_ADJECTIVE[weekNumber];
    },

    /**
     *
     * @param date {string | Date}
     * @returns {boolean}
     */
    isValidDate(date) {
        return date != null && !isNaN(new Date(date));
    },

    stringToDate(strDate) {
        if (strDate == null || strDate === "")
            return null;

        return new Date(strDate);
    },

    /**
     *
     * @param date {Date}
     */
    previousMonday(date) {
        let dayOfWeek = date.getDay();
        if (dayOfWeek === 0)
            dayOfWeek = 7;

        const msOffset = (dayOfWeek - 1) * 86_400_000;
        const previousMondayDate = new Date(date.getTime() - msOffset);

        return previousMondayDate;
    },

    /**
     *
     * @param date {Date}
     */
    nextMonday(date) {
        let dayOfWeek = date.getDay();
        if(dayOfWeek === 0)
            dayOfWeek = 7;

        const msOffset = (7 - dayOfWeek + 1) * 86_400_000;
        const nextMondayDate = new Date(date.getTime() + msOffset);

        return nextMondayDate;
    },

    /**
     * Returns a formatted string with the start date and end date.
     *
     * @param dStart The start date
     * @param dEnd The end date
     * @param separator Show the two dates if the start and end is not on the same day otherwise only one date
     * @return {string} A formatted string
     */
    formattedDate(dStart, dEnd, separator = " - ") {
        const fdStart = this.getFormattedDateDDMMYYYY(dStart);
        const fdEnd   = this.getFormattedDateDDMMYYYY(dEnd);
        if ( fdStart !== fdEnd) {
            return fdStart + separator + fdEnd;
        }

        return fdStart;
    },

    /**
     * Returns a formatted string with the start date and end date.
     * If the start date and end date are on the same day, display the day only once.
     * Otherwise, display the two date separated with the given separator
     *
     * @param dStart The start date
     * @param dEnd The end date
     * @param separator The separator between the two dates if the start and end is not on the same day
     * @return {string} A formatted string
     */
    formattedDateTime(dStart, dEnd, separator = " - ") {
        if (this.getFormattedDateDDMMYYYY(dStart) !== this.getFormattedDateDDMMYYYY(dEnd)) {
            return this.getFormattedDateDDMMYYYYHHMM(dStart) + separator + this.getFormattedDateDDMMYYYYHHMM(dEnd);
        }

        return this.getFormattedDateDDMMYYYYHHMM(dStart) + "-" + this.getFormattedHourHHMM(dEnd);
    },

    /**
     * @param dStart
     * @param dEnd
     * @param inUTC
     * @return {string}
     */
    formattedTimeRange(dStart, dEnd, inUTC = false) {
        return this.getFormattedHourHHMM(dStart, inUTC) + " - " + this.getFormattedHourHHMM(dEnd, inUTC);
    },

    /**
     * Returns true if d1 and d2 occurs the same day, false otherwise
     * @param d1 Date 1
     * @param d2 Date 2
     * @return {boolean}
     */
    sameDay(d1, d2) {
        return d1.getFullYear() === d2.getFullYear() &&
            d1.getMonth() === d2.getMonth() &&
            d1.getDate() === d2.getDate();
    },


    /**
     * Returns a date with day interval
     * @param date Date
     * @param nbDays Interval
     * @return {Date}
     */
    addDay(date, nbDays) {
        return new Date(date.getTime()+(nbDays*1000*60*60*24));
    },

    /**
     * Returns a date with month interval
     * @param date Date
     * @param nbMonths Interval
     * @return {Date}
     */
    addMonth(date, nbMonths) {
        var newDate = date;
        newDate = new Date(date.getFullYear(), date.getMonth(), 1);
        newDate.setMonth(newDate.getMonth() + (nbMonths + 1));
        newDate.setDate(newDate.getDate() - 1);

        if (date.getDate() < newDate.getDate()) {
            newDate.setDate(date.getDate());
        }
        return newDate;
    },


    /**
     * Convert a HH:MM formatted string to a number of minutes
     *
     * @param string A HH:MM formatted string
     * @return {null|*} Number of minutes
     */
    hoursStringToTime(string) {
        if (!string || string.length !== 5) return null;

        const hourMinStr = string.split(":");
        if (hourMinStr.length !== 2) return null;

        const hourMinNumber = [];
        for (const str of hourMinStr) {
            const num = Number(str);
            if (Number.isNaN(num)) {
                return null;
            }

            hourMinNumber.push(num);
        }

        return hourMinNumber[0] * 60 + hourMinNumber[1];
    },

    /**
     * Convert a time in minutes to a HH:MM formatted string
     *
     * @param time Number representing the time in minutes
     * @return {string|null}
     *
     * 264 => 04:24
     */
    timeToHoursString(time) {
        if (time == null || (typeof time !== "number" && Number.isNaN(Number(time)))) return null;

        const hours = Math.floor(time / 60);
        const minutes = time - hours * 60;

        return hours.toString().padStart(2, '0') + ":" + minutes.toString().padStart(2, '0');
    },

    /**
     *
     * @param string
     * @param minutesInterval
     */
    hoursStringAddInterval(string, minutesInterval) {
        let nbMinutes = this.hoursStringToTime(string);

        nbMinutes = Math.max(0, Math.min(24 * 60, nbMinutes + minutesInterval));

        return this.timeToHoursString(nbMinutes);
    },

    /**
     * Return a new Date based on the given date but with the given hours (in UTC)
     * @param date
     * @param minutes
     */
    setMinutes(date, minutes) {
        return new Date(new Date(date).setMinutes(minutes));
    },

    /**
     * Return a new Date based on the given date but with the given hours (in UTC)
     *
     * @param date
     * @param hours
     */
    setHours(date, hours) {
        return new Date(new Date(date).setHours(hours));
    },

    /**
     * Returns an ISO formatted string
     *
     * @param {Date | string} dateParam
     */
    toLocalIsoString(dateParam) {
        let date = new Date(dateParam);

        return (
            new Date(date.setMinutes(date.getMinutes() - date.getTimezoneOffset()))
                .toISOString()
                .replace("Z", "")
        );
    },

    /**
     * Given the {date} in the timezone {tzString}, returns a new Date at the same date/time but in the device current timezone
     *
     * ---
     *
     * Example:
     *
     * Considering the device current timezone is UTC-8:
     *   - convertTZ(new Date("2022-01-20T10:00:00.000+01:00"), "CET")
     *   will return a date at 2022-01-20T10:00:00.000-08:00
     *
     * ---
     *
     * Note that if the device current timezone is the same as tzString, the returned date will be the same as the given date param
     *
     * @param date {Date | string} the date to convert
     * @param tzString {string} an IANA-compliant timezone name
     * @return {Date}
     */
    convertTZ(date, tzString) {
        const d = (typeof date === "string" ? new Date(date) : date);
        return new Date(d.toLocaleString("en-US", {timeZone: tzString}));
    },

    /**
     * lastWeekDayOfMonth
     * @param dayOfWeek (1 for Monday, 6 for Saturday, 0 for Sunday)
     * @param month (1 for January, 12 for December)
     * @param year
     * @return {date}
     */
    lastWeekDayOfMonth(dayOfWeek, month, year) {
        const d = new Date();
        d.setFullYear(year);
        d.setDate(1); // Roll to the first day of ...
        d.setMonth(month || d.getMonth() + 1); // ... the next month.
        do { // Roll the days backwards until desired week day.
            d.setDate(d.getDate() - 1);
        } while (d.getDay() !== dayOfWeek);
        return d;
    },

    /**
     * isLastWeekDayOfMonth
     * @param date
     */
    isLastWeekDayOfMonth(date) {
        if(date == null)
            return false;

        const dayOfWeek = date.getDay();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();

        const lastWeekDayOfMonth = DateUtils.lastWeekDayOfMonth(dayOfWeek, month, year);
        if ( DateUtils.getFormattedDateYYYYMMDD(date) ===  DateUtils.getFormattedDateYYYYMMDD(lastWeekDayOfMonth))
            return true;
        return false;
    },

    /**
     * formatHoursAndMinutes
     * @param nbMinutes 270
     * @param hourLabel e.g. "Heure"
     * @param hoursLabels e.g. "Heures"
     * @param minutesLabel e.g. "Minutes"
     * @return 4 heures 30 minutes
     */
    formatHoursAndMinutes(nbMinutes, hourLabel, hoursLabels, minutesLabel) {
        let value = "";
        const nbHour = Math.floor(nbMinutes / 60);
        if (nbHour === 1)
            value = "1 " + hourLabel;
        else if (nbHour > 1)
            value = nbHour + " " + hoursLabels;

        const remainingMinutes = nbMinutes % 60;
        if (remainingMinutes > 0) {
            if (value !== "") {
                value += " ";
            }
            value += remainingMinutes + " " + minutesLabel;
        }
        return value;
    }
}

export default DateUtils;
