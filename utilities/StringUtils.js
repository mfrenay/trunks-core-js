const StringUtils = {

    //  renvoie s si non null sinon ""
    getNString(s) {
        if (s == null) return "";
        else return s;
    },

    //  renvoie s si non null ou vide sinon defaultValue
    getEString(s, defaultValue) {
        if (this.getNString(s) === "") return this.getNString(defaultValue);
        else return s;
    },

    //  renvoie l'expression dans laquelle les paramètres ont été remplacés
    replaceParametersInExpression(expression, params, del) {
        if(del == null)  {
            del = "#";
        }

        let s = expression;
        if (params != null) {
            for (let j = 1; j < params.length + 1; j++) {
                s = s.replace(new RegExp(del + j + del, "g"), this.getNString(params[j - 1]));
            }
        }
        return s;
    },

    // ltrim
    // @param s String de départ
    // @param c Caractère à supprimer
    // @return String trimée par la début
     ltrim(s, c) {
        if (c == null) c = ' ';
        if (this.getNString(s) === "")
            return "";
        s = s + "";
        while (s.length > 0) {
            if (s.charAt(0) === c)
                s = s.substring(1);
            else
                break;
        }
        return s;
    },

    // rtrim
    // @param s String de départ
    // @param c Caractère à supprimer
    // @return String trimée par la fin
     rtrim(s, c) {
        if (c == null) c = ' ';
        if (this.getNString(s) === "")
            return "";
        s = s + "";
        while (s.length > 0) {
            if (s.charAt(s.length - 1) === c)
                s = s.substring(0, s.length - 1);
            else
                break;
        }
        return s;
    },

    // lpad
    // @param s String de départ
    // @param n Nombre de caractères à atteindre
    // @param repl String qui doit compléter la String de départ par le début
    // @return String complétée
     lpad(s, n, repl) {
        if (repl == null) repl = " ";
        //alert("lpad - s = '" + s + "'");
        return this.padComplete(s, n, repl) + s;
    },

    // rpad
    // @param s String de départ
    // @param n Nombre de caractères à atteindre
    // @param repl String qui doit compléter la String de départ par la fin
    // @return String complétée
     rpad(s, n, repl) {
        if (repl == null) repl = " ";
        return s + this.padComplete(s, n, repl);
    },

    // padComplete
    // @param s String de départ
    // @param n Nombre de caractères à atteindre
    // @param repl String qui doit compléter la String de départ
    // @return Complément de la String
     padComplete(s, n, repl) {
        s = this.getNString(s) + "";
        //alert("lpad - s = '" + s + "', n = " + n + ", repl = '" + repl + "'");
        if (repl == null || repl.length === 0) {	//throw new Exception("Third parameter [repl] cannot be null nor empty");
            return s;
        }
        let s2 = "";
        //alert("s.length = '" + s.length + "'");
        let nbDiffCar = Number(n) - s.length;
        //alert("nbDiffCar = '" + nbDiffCar + "'");
        while (s2.length < nbDiffCar)
            for (let j = 0; j < repl.length && s2.length < nbDiffCar; j++)
                s2 += repl.charAt(j);
        return s2;
    },

    // replaceAll
     replaceAll(aValue, sFind, sBy) {
        while (aValue.indexOf(sFind) > -1)
            aValue = aValue.replace(sFind, sBy);
        return aValue;
    },

    /**
     * replaceSpecialChars
     * @param str A string
     * @returns {string} String in which accents have been replaced
     */
    replaceSpecialChars(str) {
        str = str.replace(/[éèêë]/g,"e");
        str = str.replace(/[àáâãäå]/g,"a");
        str = str.replace(/[ïî]/g,"i");
        str = str.replace(/[ùûü]/g,"u");
        str = str.replace(/[öô]/g,"o");
        str = str.replace(/[ç]/g,"c");

        // return
        //return str.replace(/[^a-z0-9]/gi,'');
        return str;
    },

    /**
     * Test the format of a string via a regular expression
     * @param value String
     * @param regExpPattern Pattern, i.e : ^\d{2}:\d{2}:\d{2}$
     * @returns {boolean}
     */
    isValidFormat(value, regExpPattern) {
        if (value == null) return false;

        const regex = new RegExp(regExpPattern);
        return regex.test(value);
    },

    /**
     * Return true if s correspond to a boolean value true (1, O, Y, TRUE)
     * @param s String to examinate
     * @return boolean
     */
    stringToBoolean(s) {
        if ( s != null &&
            ( s === "1" ||	s.toUpperCase() === "O" ||	s.toUpperCase() === "Y" ||	s.toUpperCase() === "TRUE" ) )
            return true;
        else
            return false;
    },

    /**
     * containsDigit
     * @param s String to examinate
     * @return {boolean} If the string contains a digit among its characters
     */
    containsDigit(s) {
        return /\d/.test(s);
    },

    /**
     * Renvoie un nombre arrondi à X décimales
     * Renvoie 0 si nombre est null
     */
    roundToXDecimal(number, X) {
        if (number == null || !(typeof number === "number")) {
            return 0;
        }

        // rounds number to X decimal places, default to 2

        //X = (!X ? 2 : X);
        if (X == null || X < 0) {
            X = 2;
        }

        return Math.round(number * Math.pow(10, X)) / Math.pow(10, X);
    },

    /**
     * Renvoie et formate un nombre arrondi à X décimales
     * Renvoie 0 si nombre est null
     *
     * @param number
     * @param X
     * @returns {*}
     */
    formatToXDecimal(number, X) {
        number = this.roundToXDecimal(number, X);
        let pos = (number + "").indexOf(".");
        const length = (number + "").length;
        if (pos === -1 && X > 0) {
            number += ".";
            pos = length - 1;
        }

        for (let k = (length - (pos + 1)); k < X; k++) {
            number += "0";
        }

        return number;
    },

    /**
     *
     * @param n
     * @param ts
     * @param ds
     * @param actu_ds
     * @param nb_dec
     * @returns {string}
     */
    formatThousandDecimalSeparator(n, ts, ds, actu_ds, nb_dec) {
        //n = n + '9999'; // test

        if (actu_ds == null)
            actu_ds = ".";

        // thousands and decimal separators
        if (ts == null)
            ts = ".";
        if (ds == null)
            ds = ",";
        if (nb_dec == null)
            nb_dec = 0;

        //alert(" ** format_thousand_decimal_separator - ts = '" + ts + "' && ds = '" + ds + "'");
        //console.log(" ** format_thousand_decimal_separator - n = '" + n + "'  - ts = '" + ts + "' && ds = '" + ds + "' - actu_ds = '" + actu_ds + "' && nb_dec = '" + nb_dec + "'");

        const ns = String(n);
        let ps = ns;
        let ss = ""; // numString, prefixString, suffixString
        const i = ns.indexOf(actu_ds);

        //console.log(" ** format_thousand_decimal_separator - n = '" + n + "'  - i = '" + i + "'");

        if (i !== -1) { // if ".", then split:
            ps = ns.substring(0, i);
            ss = ds + ns.substring(i + 1);

            if (nb_dec > 0) {
                if (ss.length > nb_dec + 1) { // erreur d'arrondi
                    ss = ss.substring(0, nb_dec + 1); // on tronque et tant pis pour les arrondis car normalement ça ne doit pas arriver
                } else {
                    while (ss.length < nb_dec + 1) {
                        ss += "0";
                    }
                }
            }
        } else { // mich/ape - 07/07/2014
            if (nb_dec > 0) {
                ss = ds;
                for (let k = 0; k < nb_dec; k++) {
                    ss += "0";
                }
            }
        }

        //console.log(" ** format_thousand_decimal_separator - n = '" + n + "'  - ss = '" + ss + "'");

        return ps.replace(/(\d)(?=(\d{3})+([.]|$))/g, "$1" + ts) + ss;
    }
};

export default StringUtils;
