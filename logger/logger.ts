export const loggerConfig = {
    /**
     * Set to true to disable the Logger to print logs, false otherwise.
     */
    disabled: process.env.NODE_ENV !== 'development',

    /**
     * Set to true to prepend logs message with the log level, false otherwise.
     */
    displayLevel: true,

    /**
     * Set to true to prepend logs message with the date and time, false otherwise.
     */
    displayTime: true,
}

enum LogLevel {
    DEBUG = "DEBUG",
    INFO = "INFO",
    WARN = "WARN",
    ERROR = "ERROR",
}

class Logger {
    static d(msg: any, ...optionalParams: any[]) {
        printMsg(console.debug, msg, LogLevel.DEBUG, ...optionalParams);
    }

    static i(msg: any, ...optionalParams: any[]) {
        printMsg(console.info, msg, LogLevel.INFO, ...optionalParams);
    }

    static w(msg: any, ...optionalParams: any[]) {
        printMsg(console.warn, msg, LogLevel.WARN, ...optionalParams);
    }

    static e(msg: any, ...optionalParams: any[]) {
        printMsg(console.error, msg, LogLevel.ERROR, ...optionalParams);
    }
}

function formattedInfoHeaders(level: string): string {
    let retMsg = "";

    if (loggerConfig.displayLevel) {
        retMsg += ` [${level}]`;
    }
    if (loggerConfig.displayTime) {
        retMsg += `[${new Date().toISOString()}]`;
    }

    return retMsg;
}

function printMsg(fn: (message?: any, ...optionalParams: any[]) => void, msg: any, level: LogLevel, ...optionalParams: any[]) {
    if (!loggerConfig.disabled) {
        fn(formattedInfoHeaders(level), msg, ...optionalParams);
    }
}

export default Logger;
