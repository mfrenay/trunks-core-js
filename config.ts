import IWebServiceManager from "./web-service/IWebServiceManager";

interface ConfigType {
    WEBAPP_URL?: string,
    BASE_API_URL?: string,
    WEB_FORM_API_URL?: string,

    webServiceManager?: IWebServiceManager,

    componentFactory: { [id: string]: any },
}

export let config: ConfigType = {
    componentFactory: {}
}

export function trunksConfigInit(newConfig: Partial<ConfigType>) {
    config = {
        ...config,
        ...newConfig
    };
}
